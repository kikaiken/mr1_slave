#ifndef DYNAMIXEL_H
#define DYNAMIXEL_H
/*
#include"periph_handler.h"
#include "stm32f7xx_hal.h"
#include "usart.h"
#include <string.h>
#include <stdio.h>

#define DYNAUART (huart2)
#define STREAMSIZE 1024
#define DYTIMEOUTMSEC (1)
#define RIGHT_HOMEPOS (230)
#define LEFT_HOMEPOS (139)



typedef enum {
	PING = 0x01,
	READ = 0x02,
	WRITE = 0x03,
	REG_WRITE = 0x04,
	ACTION = 0x05,
	FACTORY_RESET = 0x06,
	REBOOT = 0x08,
	SYNC_READ = 0x82,
	SYNC_WRITE = 0x83,
	BULK_READ = 0x92,
	BULK_WRITE = 0x93
} DynaInstruction;

typedef enum {
	Result_Fail = 7,
	Instruction_Error = 6,
	CRC_Error = 5,
	Data_Range_Error = 4,
	Data_Length_Error = 3,
	Data_Limit_Error = 2,
	Accrss_Error = 1,
	NO_Error = 0,
	Rx_Timeout = 100,
	Result_Empty = 101
}DynaError;

typedef enum {
	Model_Number = 0 ,	// uint16
	Model_Information = 2 ,	// uint32
	Version_of_Firmware = 6 ,	// uint8
	ID = 7 ,	// uint8
	Baudrate = 8 ,	// uint8
	Return_Delay_Time = 9 ,	// uint8
	Drive_Mode = 10 ,	// uint8
	Operatinng_Mode = 11 ,	// uint8
	Secondary_ID = 12 ,	// uint8
	Protocol_Version = 13 ,	// uint8
	Homing_Offset = 20 ,	// int32
	Moving_Threshold = 24 ,	// uint32
	Temperature_Limit = 31 ,	// uint8
	Max_Voltage_Limit = 32 ,	// uint16
	Min_Voltage_Limit = 34 ,	// uint16
	PWM_Limit = 36 ,	// uint16
	Current_Limit = 38 ,	// uint16
	Acceleration_Limit = 40 ,	// uint32
	Velocity_Limit = 44 ,	// uint32
	Max_Position_Limit = 48 ,	// uint32
	Min_Position_Limit = 52 ,	// uint32
	External_Port_Mode_1 = 56 ,	// uint8
	External_Port_Mode_2 = 57 ,	// uint8
	External_Port_Mode_3 = 58 ,	// uint8
	Shutdown = 63 ,	// uint8
	Torque_Enable = 64 ,	// uint8
	LED = 65 ,	// uint8
	Status_Return_Level = 68 ,	// uint8
	Registered_Instruction = 69 ,	// uint8
	Hardware_Error_Status = 70 ,	// uint8
	Velocity_I_Gain = 76 ,	// uint16
	Velocity_P_Gain = 78 ,	// uint16
	Position_D_Gain = 80 ,	// uint16
	Position_I_Gain = 82 ,	// uint16
	Position_P_Gain = 84 ,	// uint16
	Feedforward_Acceleration_Gain = 88 ,	// uint16
	Feedforward_Velocity_Gain = 90 ,	// uint16
	Bus_Watchdog = 98 ,	// int8
	Goal_PWM = 100 ,	// int16
	Goal_Current = 102 ,	// int16
	Goal_Velocity = 104 ,	// int32
	Profile_Acceleration = 108 ,	// uint32
	Profile_Velocity = 112 ,	// uint32
	Goal_Position = 116 ,	// int32
	Realtime_Tick = 120 ,	// uint16
	Moving = 122 ,	// uint8
	Moving_Status = 123 ,	// uint8
	Present_PWM = 124 ,	// int16
	Present_Current = 126 ,	// int16
	Present_Velocity = 128 ,	// int32
	Present_Position = 132 ,	// int32
	Velocity_Trajectory = 136 ,	// -
	Position_Trajectory = 140 ,	// -
	Present_Input_Voltage = 144 ,	// uint16
	Present_Temperature = 146 ,	// uint8
	External_Port_Data_1 = 152 ,	// uint16
	External_Port_Data_2 = 154 ,	// uint16
	External_Port_Data_3 = 156	// uint16
}CtrlTable;

typedef enum {
	Wait_Header,
	Wait_Length,
	Wait_Body,
	Wait_Tx,
	RxDone
}RxFlag;

typedef enum {
	Ready,
	Busy
}BusyFlag;

typedef struct {
	UART_HandleTypeDef *huart;
	volatile BusyFlag flag;
	volatile char buff[STREAMSIZE];
	volatile uint16_t txpoint;
	volatile uint16_t writepoint;
}UARTStream;


//extern const uint8_t header[3];
extern volatile uint8_t dy_rxbuff[100];
extern volatile uint8_t rxpacket[256];
extern volatile RxFlag rxpacket_flag;

void SystemClock_Config(void);
int8_t SendInstructionPacket(DynaID id, DynaInstruction inst, uint8_t* param, uint8_t param_len, uint8_t* rcv_data, DynaError* err);
unsigned short update_crc(unsigned short crc_accum, unsigned char *data_blk_ptr, unsigned short data_blk_size);
void WhenUARTSent(UART_HandleTypeDef *huart);
void WhenUARTReceived(UART_HandleTypeDef *huart);

DynaError InstPing(DynaID id, uint16_t* modelno, uint8_t* ver);
int8_t InstRead_uint8_t(DynaID id, CtrlTable addr,uint8_t* data);
int8_t InstRead_uint16_t(DynaID id, CtrlTable addr, uint16_t* data);
int8_t InstRead_uint32_t(DynaID id, CtrlTable addr, uint32_t* data);
int8_t InstRead_int32_t(DynaID id, CtrlTable addr, int32_t* data);
int8_t InstWrite_uint8_t(DynaID id, CtrlTable addr, uint8_t param, DynaError* perr);
int8_t InstWrite_uint16_t(DynaID id, CtrlTable addr, uint16_t param, DynaError* perr);
int8_t InstWrite_uint32_t(DynaID id, CtrlTable addr, uint32_t param, DynaError* perr);
int8_t InstWrite_int32_t(DynaID id, CtrlTable addr, int32_t param, DynaError* perr);

int8_t InitDynamixel(DynaID id);
int8_t SetPosition(DynaID id, uint16_t deg);
int8_t SetHandPosition(int8_t dipDeg_fromHome);
*/
#endif
