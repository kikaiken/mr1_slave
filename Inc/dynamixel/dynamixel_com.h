/*
 * dynamixel_com.h
 *
 *  Created on: 2019/03/26
 *      Author: naoki
 */

#ifndef DYNAMIXEL_DYNAMIXEL_COM_H_
#define DYNAMIXEL_DYNAMIXEL_COM_H_

#include "dynamixel/dynamixel.h"
#include <stdbool.h>

#define DYNAMIXEL_PACKET_BUF_SIZE 32
#define DYNAMIXEL_PACKET_SLOT_NUM 8

#define DYNAMIXEL_PACKET_SEND_SLOT_NUM DYNAMIXEL_PACKET_SLOT_NUM

#define DYNAMIXEL_INST_STATUS 0x55
typedef enum {
	DYNAMIXEL_COM_WAIT_SEND,
	DYNAMIXEL_COM_SENDING,
	DYNAMIXEL_COM_WAIT_RECV,
	DYNAMIXEL_COM_ACK,
	DYNAMIXEL_COM_ERROR,
	DYNAMIXEL_COM_NOT_USED,
	DYNAMIXEL_COM_TIMEOUT,
} dynamixel_com_state;

typedef enum {
	DYNAMIXEL_COM_RECV_HEADER_1,
	DYNAMIXEL_COM_RECV_HEADER_2,
	DYNAMIXEL_COM_RECV_HEADER_3,
	DYNAMIXEL_COM_RECV_RESERVED,
	DYNAMIXEL_COM_RECV_ID,
	DYNAMIXEL_COM_RECV_LEN_L,
	DYNAMIXEL_COM_RECV_LEN_H,
	DYNAMIXEL_COM_RECV_INST,
	DYNAMIXEL_COM_RECV_ERROR,
	DYNAMIXEL_COM_RECV_PARAM,
	DYNAMIXEL_COM_RECV_CRC_L,
	DYNAMIXEL_COM_RECV_CRC_H,
} dynamixel_com_recv_state;

struct dynamixel_pkt {
	dynamixel_com_state state;
	uint32_t last_sent_ms;
	dyna_inst inst;
	uint16_t len;
	uint8_t buf[DYNAMIXEL_PACKET_BUF_SIZE];
};

#define DYNAMIXEL_COM_BYTE_HEADER_1 0xFF
#define DYNAMIXEL_COM_BYTE_HEADER_2 0xFF
#define DYNAMIXEL_COM_BYTE_HEADER_3 0xFD
#define DYNAMIXEL_COM_BYTE_RESERVED 0x00
#define DYNAMIXEL_COM_BYTE_INST_ERROR 0x55

extern struct dynamixel_pkt pkt_send_slot[DYNAMIXEL_PACKET_SEND_SLOT_NUM];

void dynamixel_com_init(UART_HandleTypeDef *huart);
int dynamixel_com_available_send_slot_num();
int dynamixel_com_send_rest_num();
struct dynamixel_pkt* dynamixel_com_get_send_slot();
void dynamixel_com_send();
void dynamixel_com_send_it(UART_HandleTypeDef *huart);
void dynamixel_com_recv();
int dynamixel_com_get_w_ptr();
int dynamixel_com_get_r_ptr();
struct dynamixel_pkt* dynamixel_com_get_slot(int slot_num);
void dynamixel_com_set_timeout_retransmission(bool val);
void dynamixel_com_timeout();
void dynamixel_com_timer();

#endif /* DYNAMIXEL_DYNAMIXEL_COM_H_ */
