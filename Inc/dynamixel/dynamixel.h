/*
 * dynamixel.h
 *
 *  Created on: 2019/03/26
 *      Author: naoki
 */

#ifndef DYNAMIXEL_H_
#define DYNAMIXEL_H_

#include "main.h"

#define DYNAMIXEL_ANGLE_PARALLEL_WITH_FIREANGLE (238)//(231)
#define DYNAMIXEL_ANGLE_PARALLEL_SHOOT (206)//(184)//(207)//(235)//175
#define DYNAMIXEL_ANGLE_OFFSET_FOR_L    (0)

typedef enum {
/*
	PING = 0x01,
	READ = 0x02,
	WRITE = 0x03,
	REG_WRITE = 0x04,
	ACTION = 0x05,
	FACTORY_RESET = 0x06,
	REBOOT = 0x08,
	SYNC_READ = 0x82,
	SYNC_WRITE = 0x83,
	BULK_READ = 0x92,
	BULK_WRITE = 0x93
*/
	DYNA_INST_DUMMY,
} dyna_inst;

typedef enum{
	DynaR = 1,
	DynaL = 3,
	DynaBroad = 254
} dyna_id;

void dynamixel_init(UART_HandleTypeDef *huart);
void dynamixel_set_pos(dyna_id id, uint16_t deg);
void dynamixel_set_pos_all(double deg);
void dynamixel_op_mode(dyna_id id, uint8_t val);
void dynamixel_torque_mode(dyna_id id, uint8_t val);
void dynamixel_goal_position(dyna_id id, int32_t val);
void dynamixel_test();
void dynamixel_ping(dyna_id id);
void dynamixel_led(dyna_id, uint8_t val);
void dynamixel_connection_check();

#endif /* DYNAMIXEL_H_ */
