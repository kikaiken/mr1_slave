/*
 * motor.h
 *
 *  Created on: 2018/11/25
 *      Author: nabeya11
 */

#ifndef MOTOR_H_
#define MOTOR_H_

#include "stm32f7xx_hal.h"
#include "periph_handler.h"
#include "main.h"

//�o���u���蓖��
#define VALVE_HAND VALVE2
#define VALVE_LAUNCH VALVE1

//�o���u��Ԋ��蓖��
#define HAND_FREE VALVE_ON
#define HAND_PICK VALVE_OFF
#define LAUNCH_WAIT VALVE_ON
#define LAUNCH_START VALVE_OFF

//�o���u�ԍ�
typedef enum valve{
	VALVE1,
	VALVE2,
	VALVE3
}valve_t;

//��ʃt���O�����p�i�v�w�b�_�ړ��j
typedef enum orderstate{
	NO_ORDER,
	GOD_ORDER,
	WAIT_NEXT
}orderstate_t;

//�o���u�ėp�X�e�[�^�X
typedef enum valvestate{
	VALVE_ON = GPIO_PIN_SET,
	VALVE_OFF = GPIO_PIN_RESET
}valvestate_t;

//�T�[�{���蓖��
#define	SERVO_1 htim12
//#define	SERVO_2 htim11
//#define	SERVO_3 htim13

void Generalmotor_init(void);
void Generalmotor(uint32_t g_channel,float mspeed);

void Servomotor_init(void);
void Servomotor(TIM_HandleTypeDef *htims,uint32_t value);

void Valve(valve_t valve_num, valvestate_t valve_state);
void Valve_Toggle(valve_t valve_num);

#endif /* MOTOR_H_ */
