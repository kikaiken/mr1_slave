#ifndef RING_H
#define RING_H

#include "main.h"

#define RING_SUCCESS 0
#define RING_FAIL 1
#define DMA_PRINTF_RING_BUF_SIZE 2048
struct dma_printf_buf {
  uint8_t buf[DMA_PRINTF_RING_BUF_SIZE];
  uint16_t buf_size;
  uint16_t w_ptr, r_ptr;
  uint16_t overwrite_cnt;
};

void ring_init(struct dma_printf_buf *ring);
int ring_getc(struct dma_printf_buf *ring, uint8_t *c);
int ring_putc(struct dma_printf_buf *ring, uint8_t c);
int ring_available(struct dma_printf_buf *ring);

int ring_available_linear(struct dma_printf_buf *ring);
uint16_t ring_get_w_ptr(struct dma_printf_buf *ring);
uint16_t ring_get_r_ptr(struct dma_printf_buf *ring);

void ring_forward_r_ptr(struct dma_printf_buf *ring, int len);

void ring_set_w_ptr(struct dma_printf_buf *ring, uint16_t w_ptr);

void ring_debug(struct dma_printf_buf *ring);

#endif
