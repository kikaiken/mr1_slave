/*
 * lcd.h
 *
 *  Created on: 2018/12/08
 *      Author: Ryohei
 */

#ifndef LCD_H_
#define LCD_H_

#include <math.h>
#include <stdlib.h>
#include "main.h"
#include "stm32f7xx_hal.h"
#include "periph_handler.h"

//I2Cハンドラ
#define LCD_I2C_HANDLER		hi2c2

//LCDアドレス
#define LCD_ADDRESS			0x7C

//プロトタイプ宣言
void LCDInit(void);

#endif /* LCD_H_ */
