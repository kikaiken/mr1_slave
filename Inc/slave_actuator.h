/*
 * slave_actuator.h
 *
 *  Created on: 2019/03/25
 *      Author: Eater
 */

#ifndef SLAVE_ACTUATOR_H_
#define SLAVE_ACTUATOR_H_

#include "main.h"
#include "adc.h"
#include "dma.h"
#include "gfxsimulator.h"
#include "i2c.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

#include "motor.h"
#include <math.h>
#include "uart_com.h"
#include "dynamixel/dynamixel.h"
#include "slave_actuator.h"
#include "stdio.h"

//タイマ割り込み関係
#define TIM_PID     TIM6
#define TIM_10MS    TIM7
#define TIM_PID_HANDLER     htim6
#define TIM_10MS_HANDLER    htim7

//射角エンコーダ
#define TIM_FIREANGLE (htim2)
#define TIM_FIREANGLE_ENC   (TIM2)

//レールエンコーダ
#define TIM_RAIL (htim3)
#define TIM_RAIL_ENC (TIM3)

//エンコーダ用パラメータ
#define FIREANGLE_ENC_RADIUS    15.435//mm radius
#define RAIL_ENC_RADIUS         30.0
#define POKER_POT_RADIUS        12.0
#define NUM_OF_TEETH_OF_FIRE_ANGLE_ENCODER 50
#define NUM_OF_TEETH_OF_FIRE_ANGLE_BELT    32
#define DIR_OF_FIRE_ANGLE_ENCODER (-1)

//ポテンショの設定
#define ADC_RESOLUTION  (4096.0)
#define POTENTIO_RANGE_DEG  (3600.0)
#define ADC_CONVERTED_DATA_BUFFER_SIZE (3)

//レールエンコーダの初期値
#define RAIL_ENC_RESETVAL (30000)

//レールの位置の設定
#define RAIL_CONTRACT_LIMIT  (0)//(-1050)//(-1121)    //mm
#define RAIL_EXPAND_LIMIT  (RAIL_CONTRACT_LIMIT+596)//(-716+71)      //mm
#define RAIL_PICK   (149)//(116+87)//(-1034+71)
#define RAIL_ELEVATE (RAIL_PICK+50)

//L字の位置の設定
#define POKER_CONTRACT_LIMIT    (-673)
#define POKER_EXPAND_LIMIT      (-116)
#define POKER_PICK              (-218)

//射角エンコーダの初期値
#define FIREANGLE_ENC_RESETVAL  (20000)

//射角の位置設定
#define FIRE_ANGLE_LIMIT_DUTY   (0.80)
#define FIRE_ANGLE_DOWN_LIMIT   (-176)
//#define FIRE_ANGLE_DOWN_LIMIT_enc   (FIREANGLE_ENC_RESETVAL - 6266)    //enc tick (-100 mm)
//#define FIRE_ANGLE_UP_LIMIT_enc FIREANGLE_ENC_RESETVAL                     //enctick (-5 mm)
#define FIRE_ANGLE_UP_LIMIT (0.0)
#define FIRE_ANGLE_DOWN (-173)
#define FIRE_ANGLE_FIRE (-60)
#define FIRE_ANGLE_RAILLIMIT    (FIRE_ANGLE_DOWN + 60)
#define FIRE_ANGLE_UP   (-5)

//モータ用チャンネルの設定
#define FIREANGLECH TIM_CHANNEL_2   //Duty increase, get up
#define RAILCH      TIM_CHANNEL_4   //Duty increase, get down
#define POKERCH     TIM_CHANNEL_3   //Duty increase, expand

//リミットスイッチの設定
//LIM1:Gergehorizontal LIM2:Gergevertical LIM3:Fireangle
#define GERGEANGLE_LIMSW_PORT   LIMIT1_GPIO_Port
#define GERGEANGLE_LIMSW_PIN   LIMIT1_Pin
#define FIREANGLE_LIMSW_PORT LIMIT3_GPIO_Port
#define FIREANGLE_LIMSW_PIN LIMIT3_Pin
#define RAIL_LIMSW_PORT LIMIT5_GPIO_Port
#define RAIL_LIMSW_PIN  LIMIT5_Pin

//ゲルゲハンド用サーボの位置設定
#define Gerge_Angle_RedPassing (1050)
#define Gerge_Angle_BluePassing (1070)
#define Gerge_Angle_Red 400//1490//(900)
#define Gerge_Angle_Blue    1740//(1300)//1800//(2150)

//投擲のリリースタイミング
#define FIRE_RELEASE_MSEC   (128 + release_msec_offset)

//バルブの設定
#define FIRE_VALVE  (VALVE1)
#define SHAGAI_HAND_VALVE (VALVE2)
#define GERGE_HAND_VALVE (VALVE3)

#define RAIL_POS_OFFSET 0

//ポテンショ平滑化用リングバッファのサイズ
#define POTENTIO_RINGBUFF_SIZE (15)

#define DEADZONE_OF_POKER   (1.5)

#define NUM_OF_TRAPEZOID_POINTS (5000)


enum UARTGergeServo{
  UART_GERGESERVO_REDNORMAL = 0,
  UART_GERGESERVO_REDPASS = 1,
  UART_GERGESERVO_BLUENORMAL = 2,
  UART_GERGESERVO_BLUEPASS = 3
};

enum UARTGergeMotor{
  UART_GERGEMOTOR_UP = 0,
  UART_GERGEMOTOR_DOWN = 1
};

enum UARTFireAngle{
  UART_FIREANGLE_UP = 0,
  UART_FIREANGLE_DOWN = 1
};

enum UARTRail{
  UART_RAIL_CONTRACT = 0,
  UART_RAIL_EXPAND = 1
};


typedef enum {
  RUN,
  REST
}TrapezoidState;

typedef struct{
  float goal_pos_buff;
  int8_t is_set;
}TrapezoidBuffer;

typedef struct {
  float current_goal_val;
  float goal_val[NUM_OF_TRAPEZOID_POINTS];
  float post_goalpos;
  float preexist_goalpos;
  TrapezoidState state;
  uint16_t msec_cnt;

  float max_velocity;
  float max_acceleration;

  TrapezoidBuffer buff;
}TrapezoidVariable;

typedef enum{
  RED_FIELD,
  BLUE_FIELD,
}FieldColor;
typedef enum {
  STATE_FREEZE            = -2,
  STATE_NULL              = -1,
  STATE_RESET             =  0,
  STATE_FOREST            =  1,
  STATE_AFTER_BRIDGE       =  2,
  STATE_AFTER_LINE         =  3,
  STATE_MOVE_NEAR_SHAGAI    =  4,   //a
  STATE_MOVE_BEHIND_SHAGAI  =  5,   //b　L字を伸ばす
  STATE_LOCK_SHAGAI        =  6,   //c
  STATE_GRASP_SHAGAI       =  7,   //d
  STATE_ELEVATE_SHAGAI     =  8,   //e
  STATE_STORE_POKER        =  9,
  STATE_READY_TO_SHOOT      = 10,   //f
  STATE_SHOOT_SHAGAI       = 11,
} AttitudeState;

typedef struct{
  AttitudeState attitude;
  int8_t is_stable;
}SlaveState;

extern volatile float goal_fire_angle;                     //射角の現在目標位置
extern volatile float goal_rail_pos;                       //レールの現在目標位置
extern volatile float goal_poker_pos;                      //L字の現在目標位置

extern volatile int32_t rcvd_fire_angle;                    //デバッグ用。射角の台形加減速の目標位置を受信
extern volatile int32_t rcvd_shagai_hand_angle;             //デバッグ用。シャガイハンドの目標位置を受信

//台形加減速のための構造体
extern volatile TrapezoidVariable poker_trapezoid;
extern volatile TrapezoidVariable rail_trapezoid;
extern volatile TrapezoidVariable fire_angle_trapezoid;

extern int8_t is_fire_angle_limited;
extern int8_t is_fire_angle_set;
extern int8_t is_rail_pos_limited;
extern volatile SlaveState state;
extern FieldColor field_color;
extern volatile AttitudeState rcvd_state;
extern volatile int8_t is_initialized;

extern uint32_t current_rail_potentio;
extern volatile uint32_t rail_offset_potentio;

//割り込みで呼び出す
void When10msPassed();
void When1msPassed();
void WhenRailLimitSWPushed();
void WhenFireLimitSWPushed();

float PID(float goal_val,float current_val, float past_val, float deltamsec, float Kp, float Ki, float Kd, float* p_sumoferr, float Ki_term_limit);
void velPID(float goal_val,float current_val, float past_val, float pastpast_val, float deltamsec, float Kp, float Ki, float Kd, float* p_output);

void FireAngleMotor(float mspeed);

//Functions which begin with "Update" must be called constantly.
void UpdateShagaiFireAngle(float deg);
void UpdateRail(float pos);
void UpdatePoker(float pos);

void UpdatePokerGoalPos();


//Functions which begin with "Set" set goal positons of actuators
void SetFireAngle(float goaldeg);
void SetRail(float goalpos);
void SetPoker(float goalpos);
void SetGergeAngle(float goaldeg);

//Functions below are on-off functions.
void ShagaiFire(int8_t isset);
void ShagaiHand(int8_t isheld);

int8_t IsGergePushed();

void ThrowShagai();

void EnableFireAngleEncoder();
void EnableRailEncoder();
void GetCurrentFireAngle();
void StorePastFireAngle();
void ResetHomeFireAngle();
void ResetHomeRail();

void EnablePotentio();


void GetCurrentRailPos();
void RailMotor(float mspeed);
void StorePastRailPos();

void GetCurrentPokerPos();
void PokerMotor(float mspeed);
void StorePastPokerPos();
void SetPokerGoalPos(float goal_pos);
void InitPokerTrapezoid();
void InitRailTrapezoid();
void UpdateRailGoalPos();
void SetRailGoalPos(float goal);
void InitFireAngleTrapezoid();
void UpdateFireAngleGoalPos();
void SetFireAngleGoalPos(float goal);

void CheckAttitude();
void TransitState(AttitudeState state);

void CheckFireTimingButton();
void DisplayOnLED(uint8_t val);

#endif /* SLAVE_ACTUATOR_H_ */
