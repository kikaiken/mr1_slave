//
// Created by naoki on 19/02/23.
//


#ifndef STM32_UART_STATIC_UART_COM_H
#define STM32_UART_STATIC_UART_COM_H
#include "main.h"
#include "slave_actuator.h"

#define UART_COM_START_0 0xFF
#define UART_COM_START_1 0xFE

#define UART_COM_END_0 0x00
#define UART_COM_END_1 0x01

#define UART_COM_MAX_HANDLER 256

#define F7

#ifdef F7
#define W_PTR NDTR
#endif

#ifdef F3
#define W_PTR CNDTR
#endif
typedef union {
    uint32_t u32_val;
    float float_val;
} handler_arg;

void uart_com_send(uint8_t tag, handler_arg value);
void uart_com_send_it(UART_HandleTypeDef *huart);
void uart_com_init(UART_HandleTypeDef *huart);
void uart_com_timer();

enum uart_com_recv_st {
    UART_COM_RECV_ST_WAIT_START_0,
    UART_COM_RECV_ST_WAIT_START_1,
    UART_COM_RECV_ST_WAIT_TAG,
    UART_COM_RECV_ST_WAIT_DATA_0,
    UART_COM_RECV_ST_WAIT_DATA_1,
    UART_COM_RECV_ST_WAIT_DATA_2,
    UART_COM_RECV_ST_WAIT_DATA_3,
    UART_COM_RECV_ST_WAIT_END_0,
    UART_COM_RECV_ST_WAIT_END_1,
};

enum uart_com_handler_tag {
  UART_COM_HANDLER_State_Reset = 0,
  UART_COM_HANDLER_State_Forest = 1,
  UART_COM_HANDLER_State_AfterBridge = 2,
  UART_COM_HANDLER_State_AfterLine = 3,
  UART_COM_HANDLER_State_MoveNearShagai = 4,     //a
  UART_COM_HANDLER_State_MoveBehindShagai = 5,   //b
  UART_COM_HANDLER_State_LockShagai = 6,         //c
  UART_COM_HANDLER_State_GraspShagai = 7,          //d
  UART_COM_HANDLER_State_ElavateShagai = 8,        //e
  UART_COM_HANDLER_State_StorePoker = 9,
  UART_COM_HANDLER_State_ReadyToShoot = 10,       //f
  UART_COM_HANDLER_State_ShootShagai = 11,
  UART_COM_HANDLER_State_FREEZE = 12,
  UART_COM_HANDLER_State_Null = 13,
  UART_COM_HANDLER_Adjust_FireAngle  = 20,
  UART_COM_HANDLER_Adjust_DynaAngle  = 21,
    UART_COM_HANDLER_L_CHIKA = 110,
    UART_COM_HANDLER_ECHO_BACK = 111,
    UART_COM_HANDLER_PRINT_U32 = 112,
    UART_COM_HANDLER_COMMAND = 113,
    UART_COM_HANDLER_GERGE_SERVO = 114,
    UART_COM_HANDLER_GERGE_MOTOR = 115,
    UART_COM_HANDLER_FIREANGLE = 116,
    UART_COM_HANDLER_RAIL = 117
};

#define RING_SUCCESS 0
#define RING_FAIL 1
#define RING_BUF_SIZE 128

#define RING_TYPE_RX_NORMAL 0
#define RING_TYPE_RX_CIRCULAR 1
#define RING_TYPE_TX_NORMAL 2
struct ring_buf {
    uint8_t buf[RING_BUF_SIZE];
    uint16_t buf_size;
    uint16_t w_ptr, r_ptr;
    uint16_t overwrite_cnt;
    int type;
    UART_HandleTypeDef *huart;
};

void uart_com_ring_init(struct ring_buf *ring, UART_HandleTypeDef *huart, int type);
int uart_com_ring_getc(struct ring_buf *ring, uint8_t *c);
int uart_com_ring_putc(struct ring_buf *ring, uint8_t c);
int uart_com_ring_available(struct ring_buf *ring);
int uart_com_ring_available_linear(struct ring_buf *ring);
uint16_t uart_com_ring_get_w_ptr(struct ring_buf *ring);
uint16_t uart_com_ring_get_r_ptr(struct ring_buf *ring);
void uart_com_ring_forward_r_ptr(struct ring_buf *ring, int len);
void uart_com_ring_set_w_ptr(struct ring_buf *ring, uint16_t w_ptr);
void uart_com_ring_debug(struct ring_buf *ring);
void uart_com_proc();
typedef int (* uart_com_handler)(handler_arg arg);

int uart_com_handler_init();
int uart_com_handler_handle(uint8_t tag, handler_arg value);


#endif //STM32_UART_STATIC_UART_COM_H
