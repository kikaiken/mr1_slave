/*
 * dynamixel_com.c
 *
 *  Created on: 2019/03/26
 *      Author: naoki
 */

#include "dynamixel/dynamixel_com.h"
#include "dynamixel/dynamixel_ring.h"

#include <stdbool.h>
#include <stdio.h>

struct dynamixel_pkt pkt_send_slot[DYNAMIXEL_PACKET_SEND_SLOT_NUM];
struct dynamixel_ring_buf rx_ring;

int dynamixel_send_slot_w_ptr = 0;
int dynamixel_send_slot_r_ptr = 0;
volatile bool dynamixel_com_send_lock = false;
volatile bool dynamixel_com_timeout_retransmission = false;
volatile bool _dynamixel_com_init = false;
UART_HandleTypeDef *dynamixel_huart;

#define DYNAMIXEL_COM_TIMEOUT_MS 100

void dynamixel_com_init(UART_HandleTypeDef* huart){
	dynamixel_huart = huart;
	dynamixel_ring_init(&rx_ring, huart);

	for(int i=0;i<DYNAMIXEL_PACKET_SEND_SLOT_NUM;i++){
		pkt_send_slot[i].state = DYNAMIXEL_COM_NOT_USED;
	}

	_dynamixel_com_init = true;
}

int dynamixel_com_available_send_slot_num(){
	int used_num = 0;
	if(dynamixel_send_slot_w_ptr >= dynamixel_send_slot_r_ptr){
		used_num = dynamixel_send_slot_w_ptr - dynamixel_send_slot_r_ptr;
	}else{
		used_num = DYNAMIXEL_PACKET_SEND_SLOT_NUM + dynamixel_send_slot_w_ptr - dynamixel_send_slot_r_ptr;
	}
	return DYNAMIXEL_PACKET_SEND_SLOT_NUM - used_num;
}

int dynamixel_com_send_rest_num(){
	if(dynamixel_send_slot_r_ptr >= dynamixel_send_slot_w_ptr){
		return dynamixel_send_slot_r_ptr - dynamixel_send_slot_w_ptr;
	}else{
		return DYNAMIXEL_PACKET_SEND_SLOT_NUM + dynamixel_send_slot_r_ptr - dynamixel_send_slot_w_ptr;
	}
}


struct dynamixel_pkt* dynamixel_com_get_send_slot(){
	//log_info("SLOT NUM:%d", dynamixel_com_available_send_slot_num());
	if(dynamixel_com_available_send_slot_num() > 0){
		return &pkt_send_slot[dynamixel_send_slot_w_ptr];
	}else{
		return NULL;
	}
}

void dynamixel_com_send(){
	pkt_send_slot[dynamixel_send_slot_w_ptr].state = DYNAMIXEL_COM_WAIT_SEND;
	dynamixel_send_slot_w_ptr = (dynamixel_send_slot_w_ptr+1)%DYNAMIXEL_PACKET_SEND_SLOT_NUM;
}

void dynamixel_com_send_it(UART_HandleTypeDef *huart){
	if(huart != dynamixel_huart) return;

	if(pkt_send_slot[dynamixel_send_slot_r_ptr].state == DYNAMIXEL_COM_SENDING){
		pkt_send_slot[dynamixel_send_slot_r_ptr].last_sent_ms = HAL_GetTick();
		pkt_send_slot[dynamixel_send_slot_r_ptr].state = DYNAMIXEL_COM_WAIT_RECV;
	}
	dynamixel_com_send_lock = false;
}

void dynamixel_com_recv(){
	static dynamixel_com_recv_state st = DYNAMIXEL_COM_RECV_HEADER_1;

	static int id = 0;
	static int len = 0;
	static int inst = 0;
	static int error = 0;
	while(dynamixel_ring_available(&rx_ring) > 0){
		uint8_t data;
		dynamixel_ring_getc(&rx_ring, &data);
		switch(st){
			case DYNAMIXEL_COM_RECV_HEADER_1:
				id = 0;
				len = 0;
				inst = 0;
				error = 0;
				if(data == DYNAMIXEL_COM_BYTE_HEADER_1){
					st = DYNAMIXEL_COM_RECV_HEADER_2;
				}
				//log_info("DYNAMIXEL_COM_RECV_HEADER_1");
				break;
			case DYNAMIXEL_COM_RECV_HEADER_2:
				if(data == DYNAMIXEL_COM_BYTE_HEADER_2){
					st = DYNAMIXEL_COM_RECV_HEADER_3;
				}else{
					st = DYNAMIXEL_COM_RECV_HEADER_1;
				}
				//log_info("DYNAMIXEL_COM_RECV_HEADER_2");
				break;
			case DYNAMIXEL_COM_RECV_HEADER_3:
				if(data == DYNAMIXEL_COM_BYTE_HEADER_3){
					st = DYNAMIXEL_COM_RECV_RESERVED;
				}else{
					st = DYNAMIXEL_COM_RECV_HEADER_1;
				}
				//log_info("DYNAMIXEL_COM_RECV_HEADER_3");
				break;
			case DYNAMIXEL_COM_RECV_RESERVED:
				if(data == DYNAMIXEL_COM_BYTE_RESERVED){
					st = DYNAMIXEL_COM_RECV_ID;
				}else{
					st = DYNAMIXEL_COM_RECV_HEADER_1;
				}
				//log_info("DYNAMIXEL_COM_RESERVED");
				break;
			case DYNAMIXEL_COM_RECV_ID:
				id = data;
				st = DYNAMIXEL_COM_RECV_LEN_L;
				//log_info("DYNAMIXEL_COM_RECV_ID");
				break;
			case DYNAMIXEL_COM_RECV_LEN_L:
				len = data;
				st = DYNAMIXEL_COM_RECV_LEN_H;
				//log_info("DYNAMIXEL_COM_RECV_LEN_L");
				break;
			case DYNAMIXEL_COM_RECV_LEN_H:
				len += data<<8;
				st = DYNAMIXEL_COM_RECV_INST;
				//log_info("DYNAMIXEL_COM_RECV_LEN_H");
				break;
			case DYNAMIXEL_COM_RECV_INST:
				inst = data;
				len--;
				if(inst == DYNAMIXEL_INST_STATUS){
					st = DYNAMIXEL_COM_RECV_ERROR;
				}else{
					if(len < 3){
						st = DYNAMIXEL_COM_RECV_CRC_L;
					}else{
						st = DYNAMIXEL_COM_RECV_PARAM;
					}
				}
				//log_info("DYNAMIXEL_COM_RECV_INST");
				break;
			case DYNAMIXEL_COM_RECV_ERROR:
				len--;
				error = data;
				//log_info("DYNAMIXEL_COM_RECV_ERROR");
				if(len < 3){
					st = DYNAMIXEL_COM_RECV_CRC_L;
				}else{
					st = DYNAMIXEL_COM_RECV_PARAM;
				}
				break;
			case DYNAMIXEL_COM_RECV_PARAM:
				len--;
				//log_info("DYNAMIXEL_COM_RECV_PARAM");
				if(len < 3){
					st = DYNAMIXEL_COM_RECV_CRC_L;
				}
				break;
			case DYNAMIXEL_COM_RECV_CRC_L:
				st = DYNAMIXEL_COM_RECV_CRC_H;
				//log_info("DYNAMIXEL_COM_RECV_CRC_L");
				break;
			case DYNAMIXEL_COM_RECV_CRC_H:
				//log_info("DYNAMIXEL_COM_RECV_CRC_H");
				log_info("id:%d inst:%d",id ,inst);
				st = DYNAMIXEL_COM_RECV_HEADER_1;
				if(pkt_send_slot[dynamixel_send_slot_r_ptr].state == DYNAMIXEL_COM_WAIT_RECV && error == 0 && inst == DYNAMIXEL_INST_STATUS){
					pkt_send_slot[dynamixel_send_slot_r_ptr].state = DYNAMIXEL_COM_ACK;
					dynamixel_send_slot_r_ptr = (dynamixel_send_slot_r_ptr+1)%DYNAMIXEL_PACKET_SEND_SLOT_NUM;
					log_info("Dynamixel Recv ACK");
				}else if(pkt_send_slot[dynamixel_send_slot_r_ptr].state == DYNAMIXEL_COM_WAIT_RECV && inst == DYNAMIXEL_INST_STATUS){
					pkt_send_slot[dynamixel_send_slot_r_ptr].state = DYNAMIXEL_COM_WAIT_RECV;
					dynamixel_send_slot_r_ptr = (dynamixel_send_slot_r_ptr+1)%DYNAMIXEL_PACKET_SEND_SLOT_NUM;
					log_info("Dynamixel Recv Error:%d", error);
				}
				break;
		}
	}
}

void dynamixel_com_timeout(){
	struct dynamixel_pkt *slot = &pkt_send_slot[dynamixel_send_slot_r_ptr];
	if(slot->state == DYNAMIXEL_COM_WAIT_RECV && HAL_GetTick()-slot->last_sent_ms > DYNAMIXEL_COM_TIMEOUT_MS){
		if(dynamixel_com_timeout_retransmission){
			slot->state = DYNAMIXEL_COM_WAIT_SEND;
			log_info("Dynamixel timeout. Retrying sending...");
		}else{
			slot->state = DYNAMIXEL_COM_TIMEOUT;
			dynamixel_send_slot_r_ptr = (dynamixel_send_slot_r_ptr+1)%DYNAMIXEL_PACKET_SEND_SLOT_NUM;
			log_info("Dynamixel timeout. Ignoring packet.");
		}
	}
}

int dynamixel_com_get_r_ptr(){
	return dynamixel_send_slot_r_ptr;
}

int dynamixel_com_get_w_ptr(){
	return dynamixel_send_slot_w_ptr;
}

void dynamixel_com_set_timeout_retransmission(bool val){
	dynamixel_com_timeout_retransmission = val;
}
struct dynamixel_pkt* dynamixel_com_get_slot(int slot_num){
	return &pkt_send_slot[slot_num];
}
void dynamixel_com_timer(){
	if(!_dynamixel_com_init) return;

	dynamixel_com_timeout();
	if(dynamixel_com_send_rest_num() > 0 && pkt_send_slot[dynamixel_send_slot_r_ptr].state == DYNAMIXEL_COM_WAIT_SEND && dynamixel_com_send_lock == false){
		dynamixel_com_send_lock = true;
		HAL_UART_Transmit_DMA(dynamixel_huart, pkt_send_slot[dynamixel_send_slot_r_ptr].buf, pkt_send_slot[dynamixel_send_slot_r_ptr].len);
		pkt_send_slot[dynamixel_send_slot_r_ptr].state = DYNAMIXEL_COM_SENDING;
	}
	dynamixel_com_recv();
}
