/*
 * dynamixel_ring.c
 *
 *  Created on: 2019/03/26
 *      Author: naoki
 */
#include "main.h"
#include "dynamixel/dynamixel_ring.h"

static void update_w_ptr(struct dynamixel_ring_buf *ring){
	ring->w_ptr = (uint16_t)((ring->buf_size - ring->huart->hdmarx->Instance->NDTR)&0xFFFF);
}

void dynamixel_ring_init(struct dynamixel_ring_buf *ring, UART_HandleTypeDef *huart){
  ring->buf_size = DYNAMIXEL_RING_BUF_SIZE;
  ring->w_ptr = 0;
  ring->r_ptr = 0;
  ring->overwrite_cnt = 0;
  ring->huart = huart;

  HAL_UART_Receive_DMA(ring->huart, ring->buf, ring->buf_size);
}

int dynamixel_ring_getc(struct dynamixel_ring_buf *ring, uint8_t *c){
	update_w_ptr(ring);
	if(ring->r_ptr == ring->w_ptr) return RING_FAIL;
	uint16_t next_ptr = ring->r_ptr+1;
	if(next_ptr >= ring->buf_size) next_ptr = 0;

  *c = ring->buf[ring->r_ptr];
  ring->r_ptr = next_ptr;
  return RING_SUCCESS;
}

int dynamixel_ring_putc(struct dynamixel_ring_buf *ring, uint8_t c){
	update_w_ptr(ring);

  uint16_t next_ptr = ring->w_ptr+1;
  if(next_ptr >= ring->buf_size) next_ptr = 0;

  if(next_ptr == ring->r_ptr){
    ring->overwrite_cnt++;
    return RING_FAIL;
  }
  ring->buf[ring->w_ptr] = c;
  ring->w_ptr = next_ptr;
  return RING_SUCCESS;
}

int dynamixel_ring_available(struct dynamixel_ring_buf *ring){
	update_w_ptr(ring);

  if(ring->w_ptr >= ring->r_ptr){
    return ring->w_ptr - ring->r_ptr;
  }else{
    return ring->buf_size + ring->w_ptr - ring->r_ptr;
  }
}
