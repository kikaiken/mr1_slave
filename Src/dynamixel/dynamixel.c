/*
 * dynamixel.c
 *
 *  Created on: 2019/03/26
 *      Author: naoki
 */

#include "dynamixel/dynamixel.h"
#include "dynamixel/dynamixel_com.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>


#define DYNAMIXEL_RIGHT_ANGLE_PARALLEL_WTIH_FIREANGLE (215)
#define DYNAMIXEL_LEFT_ANGLE_PARALLEL_WTIH_FIREANGLE (145)

const static uint8_t send_pkt_header[5] = {0xff, 0xff, 0xfd, 0x00, 0x00};
static uint16_t update_crc(uint16_t crc_accum, uint8_t *data_blk_ptr, uint16_t data_blk_size)
{
    uint16_t i, j;
    uint16_t crc_table[256] = {
        0x0000, 0x8005, 0x800F, 0x000A, 0x801B, 0x001E, 0x0014, 0x8011,
        0x8033, 0x0036, 0x003C, 0x8039, 0x0028, 0x802D, 0x8027, 0x0022,
        0x8063, 0x0066, 0x006C, 0x8069, 0x0078, 0x807D, 0x8077, 0x0072,
        0x0050, 0x8055, 0x805F, 0x005A, 0x804B, 0x004E, 0x0044, 0x8041,
        0x80C3, 0x00C6, 0x00CC, 0x80C9, 0x00D8, 0x80DD, 0x80D7, 0x00D2,
        0x00F0, 0x80F5, 0x80FF, 0x00FA, 0x80EB, 0x00EE, 0x00E4, 0x80E1,
        0x00A0, 0x80A5, 0x80AF, 0x00AA, 0x80BB, 0x00BE, 0x00B4, 0x80B1,
        0x8093, 0x0096, 0x009C, 0x8099, 0x0088, 0x808D, 0x8087, 0x0082,
        0x8183, 0x0186, 0x018C, 0x8189, 0x0198, 0x819D, 0x8197, 0x0192,
        0x01B0, 0x81B5, 0x81BF, 0x01BA, 0x81AB, 0x01AE, 0x01A4, 0x81A1,
        0x01E0, 0x81E5, 0x81EF, 0x01EA, 0x81FB, 0x01FE, 0x01F4, 0x81F1,
        0x81D3, 0x01D6, 0x01DC, 0x81D9, 0x01C8, 0x81CD, 0x81C7, 0x01C2,
        0x0140, 0x8145, 0x814F, 0x014A, 0x815B, 0x015E, 0x0154, 0x8151,
        0x8173, 0x0176, 0x017C, 0x8179, 0x0168, 0x816D, 0x8167, 0x0162,
        0x8123, 0x0126, 0x012C, 0x8129, 0x0138, 0x813D, 0x8137, 0x0132,
        0x0110, 0x8115, 0x811F, 0x011A, 0x810B, 0x010E, 0x0104, 0x8101,
        0x8303, 0x0306, 0x030C, 0x8309, 0x0318, 0x831D, 0x8317, 0x0312,
        0x0330, 0x8335, 0x833F, 0x033A, 0x832B, 0x032E, 0x0324, 0x8321,
        0x0360, 0x8365, 0x836F, 0x036A, 0x837B, 0x037E, 0x0374, 0x8371,
        0x8353, 0x0356, 0x035C, 0x8359, 0x0348, 0x834D, 0x8347, 0x0342,
        0x03C0, 0x83C5, 0x83CF, 0x03CA, 0x83DB, 0x03DE, 0x03D4, 0x83D1,
        0x83F3, 0x03F6, 0x03FC, 0x83F9, 0x03E8, 0x83ED, 0x83E7, 0x03E2,
        0x83A3, 0x03A6, 0x03AC, 0x83A9, 0x03B8, 0x83BD, 0x83B7, 0x03B2,
        0x0390, 0x8395, 0x839F, 0x039A, 0x838B, 0x038E, 0x0384, 0x8381,
        0x0280, 0x8285, 0x828F, 0x028A, 0x829B, 0x029E, 0x0294, 0x8291,
        0x82B3, 0x02B6, 0x02BC, 0x82B9, 0x02A8, 0x82AD, 0x82A7, 0x02A2,
        0x82E3, 0x02E6, 0x02EC, 0x82E9, 0x02F8, 0x82FD, 0x82F7, 0x02F2,
        0x02D0, 0x82D5, 0x82DF, 0x02DA, 0x82CB, 0x02CE, 0x02C4, 0x82C1,
        0x8243, 0x0246, 0x024C, 0x8249, 0x0258, 0x825D, 0x8257, 0x0252,
        0x0270, 0x8275, 0x827F, 0x027A, 0x826B, 0x026E, 0x0264, 0x8261,
        0x0220, 0x8225, 0x822F, 0x022A, 0x823B, 0x023E, 0x0234, 0x8231,
        0x8213, 0x0216, 0x021C, 0x8219, 0x0208, 0x820D, 0x8207, 0x0202
    };

    for(j = 0; j < data_blk_size; j++)
    {
        i = ((uint16_t)(crc_accum >> 8) ^ data_blk_ptr[j]) & 0xFF;
        crc_accum = (crc_accum << 8) ^ crc_table[i];
    }

    return crc_accum;
}

static void _write(dyna_id id, uint16_t addr, uint8_t *param, uint16_t param_len){
	struct dynamixel_pkt *pkt = dynamixel_com_get_send_slot();
	if(pkt == NULL) return;

	uint8_t *data = pkt->buf;
	uint16_t len = 3+2+param_len;

	memcpy(data, send_pkt_header, 4);
	data[4] = id;
	data[5] = (uint8_t)(len&0xFF);
	data[6] = (uint8_t)((len>>8)&0xFF);
	data[7] = 0x03;
	data[8] = (uint8_t)(addr&0xFF);
	data[9] = (uint8_t)((addr>>8)&0xFF);
	memcpy(&data[10], param, param_len);

	uint16_t crc = update_crc(0, data, 10+param_len);

	data[10+param_len] = (uint8_t)(crc&0xFF);
	data[11+param_len] = (uint8_t)((crc>>8)&0xFF);
	pkt->len = 12+param_len;
	dynamixel_com_send();
}

void dynamixel_init(UART_HandleTypeDef *huart){
	log_info("Initializing Dynamixel...");

	dynamixel_com_init(huart);

	dynamixel_com_set_timeout_retransmission(true);
	dynamixel_connection_check();
	dynamixel_op_mode(DynaR, 3);
	dynamixel_op_mode(DynaL, 3);
	dynamixel_set_pos_all(DYNAMIXEL_ANGLE_PARALLEL_WITH_FIREANGLE);

}

void dynamixel_test(){
	log_info("Starting Dynamixel Test...");
	int led = 1;
	int deg = 180;
	while(1){
		dynamixel_led(DynaR, led);
		dynamixel_set_pos(DynaR, deg);
		HAL_Delay(10);
		dynamixel_led(DynaL, led);
		dynamixel_set_pos(DynaL, deg);
		HAL_Delay(2500);
		led = (led+1)%2;
		if(deg == 225){
			deg = 135;
		}else{
			deg = 225;
		}

	}
}

void dynamixel_set_pos(dyna_id id, uint16_t deg){
	  int32_t pos;
	  if(deg >= 360) deg =359;
	  pos = deg * 4096 /360;
	  dynamixel_torque_mode(id, 1);
	  dynamixel_goal_position(id, pos);
}

void dynamixel_set_pos_all(double deg){
	double dynaR_deg = deg;
    double dynaL_deg = 360-(deg + DYNAMIXEL_ANGLE_OFFSET_FOR_L);
	if(dynaR_deg >= 360) dynaR_deg = 359;
	if(dynaL_deg >= 360) dynaL_deg = 359;

	uint16_t dynaR_pos = (uint16_t)(dynaR_deg * 4096.0 /360.0);
	uint16_t dynaL_pos = (uint16_t)(dynaL_deg * 4096.0 / 360.0);

	dynamixel_torque_mode(DynaR, 1);
	dynamixel_goal_position(DynaR, dynaR_pos);
	dynamixel_torque_mode(DynaL, 1);
	dynamixel_goal_position(DynaL, dynaL_pos);
}
void dynamixel_op_mode(dyna_id id, uint8_t val){
	log_info("Sending Operating Mode...");
	_write(id, 11, &val, 1);
}

void dynamixel_torque_mode(dyna_id id, uint8_t val){
	log_info("Sending Torque Mode...");
	_write(id, 64, &val, 1);
}

void dynamixel_goal_position(dyna_id id, int32_t val){
	log_info("Sending Goal Position...");
	uint8_t tmp[4] = {(uint8_t)(val&0xFF), (uint8_t)((val>>8)&0xFF), (uint8_t)((val>>16)&0xFF), (uint8_t)((val>>24)&0xFF)};
	_write(id, 116, tmp, 4);
}
void dynamixel_ping(dyna_id id){
	struct dynamixel_pkt *pkt = dynamixel_com_get_send_slot();
	if(pkt == NULL) return;
	log_info("Sending Ping...");
	uint8_t *data = pkt->buf;
	uint16_t len = 3;
	memcpy(data, send_pkt_header, 4);
	data[4] = id;
	data[5] = (uint8_t)(len&0xFF);
	data[6] = (uint8_t)((len>>8)&0xFF);
	data[7] = 0x01;
	uint16_t crc = update_crc(0, data, 8);
	data[8] = (uint8_t)(crc&0xFF);
	data[9] = (uint8_t)((crc>>8)&0xFF);

	pkt->len = 10;
	dynamixel_com_send();
}

void dynamixel_led(dyna_id id, uint8_t val){
	log_info("Sending LED write...");
	_write(id, 65, &val, 1);
}

void dynamixel_connection_check(){
	dynamixel_com_set_timeout_retransmission(true);

	int w_ptr = dynamixel_com_get_w_ptr();
	dynamixel_ping(DynaR);
	volatile struct dynamixel_pkt *slot = dynamixel_com_get_slot(w_ptr);
	while(slot->state != DYNAMIXEL_COM_ACK){
		HAL_Delay(1);
	}
	log_info("Dynamixel DynaR successfully connected");

	w_ptr = dynamixel_com_get_w_ptr();
	dynamixel_ping(DynaL);
	slot = dynamixel_com_get_slot(w_ptr);
	while(slot->state != DYNAMIXEL_COM_ACK){
		HAL_Delay(1);
	}
	log_info("Dynamixel DynaL successfully connected");
}
