#include "dma_printf/dma_printf_ring.h"
#include <stdio.h>
#include "main.h"

void ring_init(struct dma_printf_buf *ring){
  ring->buf_size = DMA_PRINTF_RING_BUF_SIZE;
  ring->w_ptr = 0;
  ring->r_ptr = 0;
  ring->overwrite_cnt = 0;
}

int ring_getc(struct dma_printf_buf *ring, uint8_t *c){
  if(ring->r_ptr == ring->w_ptr) return RING_FAIL;
  uint16_t next_ptr = ring->r_ptr+1;
  if(next_ptr >= ring->buf_size) next_ptr = 0;

  *c = ring->buf[ring->r_ptr];
  ring->r_ptr = next_ptr;
  return RING_SUCCESS;
}

int ring_putc(struct dma_printf_buf *ring, uint8_t c){
  uint16_t next_ptr = ring->w_ptr+1;
  if(next_ptr >= ring->buf_size) next_ptr = 0;

  if(next_ptr == ring->r_ptr){
    ring->overwrite_cnt++;
    return RING_FAIL;
  }
  ring->buf[ring->w_ptr] = c;
  ring->w_ptr = next_ptr;
  return RING_SUCCESS;
}

int ring_available(struct dma_printf_buf *ring){
  if(ring->w_ptr >= ring->r_ptr){
    return ring->w_ptr - ring->r_ptr;
  }else{
    return ring->buf_size + ring->w_ptr - ring->r_ptr;
  }
}
int ring_available_linear(struct dma_printf_buf *ring){
  if(ring->w_ptr >= ring->r_ptr){
    return ring->w_ptr - ring->r_ptr;
  }else{
    return ring->buf_size - ring->r_ptr;
  }
}

uint16_t ring_get_w_ptr(struct dma_printf_buf *ring){
  return ring->w_ptr;
}

uint16_t ring_get_r_ptr(struct dma_printf_buf *ring){
  return ring->r_ptr;
}

void ring_forward_r_ptr(struct dma_printf_buf *ring, int len){
  while(len > 0){
    if(ring->r_ptr+1 >= ring->buf_size){
      ring->r_ptr = 0;
    }else{
      ring->r_ptr += 1;
    }
    len--;
  }
}

void ring_set_w_ptr(struct dma_printf_buf *ring, uint16_t w_ptr){
    ring->w_ptr = w_ptr;
}
void ring_debug(struct dma_printf_buf *ring){
  printf("\n====Ring Debug information====\n");
    printf("Buffer Size: %d\n", ring->buf_size);
    printf("Write Pointer: %d\n", ring->w_ptr);
    printf("Read Pointer: %d\n", ring->r_ptr);
    printf("OverWrite Count: %d\n\n", ring->overwrite_cnt);
}
