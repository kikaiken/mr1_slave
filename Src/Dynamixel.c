#include "Dynamixel.h"
/*
volatile uint8_t header[5] = {0xff, 0xff, 0xfd, 0x00, 0x00};
volatile uint8_t dy_rxbuff[100];
volatile uint8_t header_num = 0;
volatile uint8_t rxpacket[256];
volatile RxFlag rxpacket_flag = RxDone;
volatile uint8_t rxpacket_len;
char str[256];
uint8_t txpacket[100];
UARTStream debugstream = {&huart3,Ready,"\0",0,0};
volatile uint32_t dy_buswait = 0;
volatile uint8_t* received_data;
volatile DynaError* received_err;
volatile uint8_t buff_offset;


unsigned short update_crc(unsigned short crc_accum, unsigned char *data_blk_ptr, unsigned short data_blk_size)
{
    unsigned short i, j;
    unsigned short crc_table[256] = {
        0x0000, 0x8005, 0x800F, 0x000A, 0x801B, 0x001E, 0x0014, 0x8011,
        0x8033, 0x0036, 0x003C, 0x8039, 0x0028, 0x802D, 0x8027, 0x0022,
        0x8063, 0x0066, 0x006C, 0x8069, 0x0078, 0x807D, 0x8077, 0x0072,
        0x0050, 0x8055, 0x805F, 0x005A, 0x804B, 0x004E, 0x0044, 0x8041,
        0x80C3, 0x00C6, 0x00CC, 0x80C9, 0x00D8, 0x80DD, 0x80D7, 0x00D2,
        0x00F0, 0x80F5, 0x80FF, 0x00FA, 0x80EB, 0x00EE, 0x00E4, 0x80E1,
        0x00A0, 0x80A5, 0x80AF, 0x00AA, 0x80BB, 0x00BE, 0x00B4, 0x80B1,
        0x8093, 0x0096, 0x009C, 0x8099, 0x0088, 0x808D, 0x8087, 0x0082,
        0x8183, 0x0186, 0x018C, 0x8189, 0x0198, 0x819D, 0x8197, 0x0192,
        0x01B0, 0x81B5, 0x81BF, 0x01BA, 0x81AB, 0x01AE, 0x01A4, 0x81A1,
        0x01E0, 0x81E5, 0x81EF, 0x01EA, 0x81FB, 0x01FE, 0x01F4, 0x81F1,
        0x81D3, 0x01D6, 0x01DC, 0x81D9, 0x01C8, 0x81CD, 0x81C7, 0x01C2,
        0x0140, 0x8145, 0x814F, 0x014A, 0x815B, 0x015E, 0x0154, 0x8151,
        0x8173, 0x0176, 0x017C, 0x8179, 0x0168, 0x816D, 0x8167, 0x0162,
        0x8123, 0x0126, 0x012C, 0x8129, 0x0138, 0x813D, 0x8137, 0x0132,
        0x0110, 0x8115, 0x811F, 0x011A, 0x810B, 0x010E, 0x0104, 0x8101,
        0x8303, 0x0306, 0x030C, 0x8309, 0x0318, 0x831D, 0x8317, 0x0312,
        0x0330, 0x8335, 0x833F, 0x033A, 0x832B, 0x032E, 0x0324, 0x8321,
        0x0360, 0x8365, 0x836F, 0x036A, 0x837B, 0x037E, 0x0374, 0x8371,
        0x8353, 0x0356, 0x035C, 0x8359, 0x0348, 0x834D, 0x8347, 0x0342,
        0x03C0, 0x83C5, 0x83CF, 0x03CA, 0x83DB, 0x03DE, 0x03D4, 0x83D1,
        0x83F3, 0x03F6, 0x03FC, 0x83F9, 0x03E8, 0x83ED, 0x83E7, 0x03E2,
        0x83A3, 0x03A6, 0x03AC, 0x83A9, 0x03B8, 0x83BD, 0x83B7, 0x03B2,
        0x0390, 0x8395, 0x839F, 0x039A, 0x838B, 0x038E, 0x0384, 0x8381,
        0x0280, 0x8285, 0x828F, 0x028A, 0x829B, 0x029E, 0x0294, 0x8291,
        0x82B3, 0x02B6, 0x02BC, 0x82B9, 0x02A8, 0x82AD, 0x82A7, 0x02A2,
        0x82E3, 0x02E6, 0x02EC, 0x82E9, 0x02F8, 0x82FD, 0x82F7, 0x02F2,
        0x02D0, 0x82D5, 0x82DF, 0x02DA, 0x82CB, 0x02CE, 0x02C4, 0x82C1,
        0x8243, 0x0246, 0x024C, 0x8249, 0x0258, 0x825D, 0x8257, 0x0252,
        0x0270, 0x8275, 0x827F, 0x027A, 0x826B, 0x026E, 0x0264, 0x8261,
        0x0220, 0x8225, 0x822F, 0x022A, 0x823B, 0x023E, 0x0234, 0x8231,
        0x8213, 0x0216, 0x021C, 0x8219, 0x0208, 0x820D, 0x8207, 0x0202
    };

    for(j = 0; j < data_blk_size; j++)
    {
        i = ((unsigned short)(crc_accum >> 8) ^ data_blk_ptr[j]) & 0xFF;
        crc_accum = (crc_accum << 8) ^ crc_table[i];
    }

    return crc_accum;
}

int8_t SendInstructionPacket(DynaID id, DynaInstruction inst, uint8_t* param, uint8_t param_len, uint8_t* rcv_data, DynaError* err){
	uint16_t length = param_len+3;
	uint16_t crc;
	uint32_t delta = HAL_GetTick() - dy_buswait;

	if(delta > DYTIMEOUTMSEC){
		dy_buswait = 0;
		rxpacket_flag = RxDone;
		*received_err = Rx_Timeout;
		header_num = 0;
	}

	if(rxpacket_flag != RxDone){
		return -1;
	}

	for(uint8_t i = 0; i < 3; i++){
		txpacket[i] = header[i];
	}

	txpacket[3] = 0x00;
	txpacket[4] = (uint8_t) id;
	txpacket[5] = (length&0x00ff);
	txpacket[6] = (length&0xff00) >> 8;
	txpacket[7] = (uint8_t) inst;

	if(param_len != 0 && param != NULL)
		for(uint8_t i = 8; i < 8+param_len; i++){
			txpacket[i] = param[i-8];
		}

	crc = update_crc(0, txpacket, 8+param_len);

	txpacket[8+param_len] = crc&0x00ff;
	txpacket[9+param_len] = (crc&0xff00) >> 8;

	header[4] = id;
	HAL_UART_Transmit_DMA(&DYNAUART, (uint8_t*)txpacket, 10 +param_len);
	HAL_UART_Receive_DMA(&DYNAUART, (uint8_t*)dy_rxbuff, 10 +param_len);
	rxpacket_flag = Wait_Tx;
	received_data = rcv_data;
	received_err = err;
	*received_err = Result_Empty;
	dy_buswait = HAL_GetTick();
	return 0;
}

DynaError InstPing(DynaID id, uint16_t* modelno, uint8_t* ver){
	uint8_t ping[3];
	DynaError err;
	uint16_t cnt = 0;
	while(rxpacket_flag!=RxDone){
		if(HAL_GetTick() - dy_buswait > DYTIMEOUTMSEC){
			return Rx_Timeout;
		}
	}
	SendInstructionPacket(id, PING, NULL, 0, ping, &err);
	while(rxpacket_flag!=RxDone){
		if(HAL_GetTick() - dy_buswait > DYTIMEOUTMSEC){
			return Rx_Timeout;
		}
	}
	*modelno = ping[0]|(ping[1]<<8);
	*ver = ping[2];
	return err;
}
int8_t InstRead_uint8_t(DynaID id, CtrlTable addr,uint8_t* data){
	uint8_t bytes[4];
	DynaError err;
	bytes[0] = addr & 0x00ff;
	bytes[1] = (addr & 0xff00) >> 8;
	bytes[2] = 1;
	bytes[3] = 0;
	while(SendInstructionPacket(id, WRITE, bytes, 4, data, &err)!=0);
	while(dy_buswait!=0);
	return err;
}
int8_t InstRead_uint16_t(DynaID id, CtrlTable addr, uint16_t* data){
	uint8_t bytes[4];
	uint8_t rcv[2];
	DynaError err;
	bytes[0] = addr & 0x00ff;
	bytes[1] = (addr & 0xff00) >> 8;
	bytes[2] = 2;
	bytes[3] = 0;
	while(SendInstructionPacket(id, WRITE, bytes, 4, rcv, &err)!=0);
	while(dy_buswait!=0);
	*data = 0;
	for(uint8_t i = 0; i<2; i++)
		*data|= rcv[i]<<(8*i);
	return 0;
}
int8_t InstRead_uint32_t(DynaID id, CtrlTable addr, uint32_t* data){
	uint8_t bytes[4];
	uint8_t rcv[4];
	DynaError err;
	bytes[0] = addr & 0x00ff;
	bytes[1] = (addr & 0xff00) >> 8;
	bytes[2] = 4;
	bytes[3] = 0;
	while(SendInstructionPacket(id, WRITE, bytes, 4, rcv, &err)!=0);
	while(dy_buswait!=0);
	*data = 0;
	for(uint8_t i = 0; i<4; i++)
		*data|= rcv[i]<<(8*i);
	return 0;
}
int8_t InstRead_int32_t(DynaID id, CtrlTable addr, int32_t* data){
	uint8_t bytes[4];
	uint8_t rcv[4];
	DynaError err;
	bytes[0] = addr & 0x00ff;
	bytes[1] = (addr & 0xff00) >> 8;
	bytes[2] = 4;
	bytes[3] = 0;
	while(SendInstructionPacket(id, WRITE, bytes, 4, rcv, &err)!=0);
	while(dy_buswait!=0);
	*data = 0;
	for(uint8_t i = 0; i<4; i++)
		*data|= rcv[i]<<(8*i);
	return 0;
}
int8_t InstWrite_uint8_t(DynaID id, CtrlTable addr, uint8_t param, DynaError* perr){
	uint8_t bytes[3];
	bytes[0] = addr & 0x00ff;
	bytes[1] = (addr & 0xff00) >> 8;
	bytes[2] = param;
	return SendInstructionPacket(id, WRITE, bytes, 3, NULL, perr);
}
int8_t InstWrite_uint16_t(DynaID id, CtrlTable addr, uint16_t param, DynaError* perr){
	uint8_t bytes[4];
	bytes[0] = addr & 0x00ff;
	bytes[1] = (addr & 0xff00) >> 8;
	for(uint8_t i = 0; i < 2; i++)
		bytes[2+i] = (param&(0xff<<(8*i)))>>(i*8);
	return SendInstructionPacket(id, WRITE, bytes, 4, NULL, perr);
}
int8_t InstWrite_uint32_t(DynaID id, CtrlTable addr, uint32_t param, DynaError* perr){
	uint8_t bytes[6];
	bytes[0] = addr & 0x00ff;
	bytes[1] = (addr & 0xff00) >> 8;
	for(uint8_t i = 0; i < 4; i++)
		bytes[2+i] = (param&(0xff<<(8*i)))>>(i*8);
	return SendInstructionPacket(id, WRITE, bytes, 6, NULL, perr);
}
int8_t InstWrite_int32_t(DynaID id, CtrlTable addr, int32_t param, DynaError* perr){
	uint8_t bytes[6];
	bytes[0] = addr & 0x00ff;
	bytes[1] = (addr & 0xff00) >> 8;
	for(uint8_t i = 0; i < 4; i++)
		bytes[2+i] = (param&(0xff<<(8*i)))>>(i*8);
	return SendInstructionPacket(id, WRITE, bytes, 6, NULL, perr);

}

void WhenUARTReceived(UART_HandleTypeDef *huart){
    if(huart == &DYNAUART){

		if(HAL_GetTick() - dy_buswait > DYTIMEOUTMSEC){
			rxpacket_flag =RxDone;
			dy_buswait = 0;
			header_num = 0;
			*received_err = Rx_Timeout;

			return;
		}
    	switch(rxpacket_flag){
    	case Wait_Header:
    		rxpacket[header_num] = dy_rxbuff[0];
    		if(rxpacket[header_num] == header[header_num]){
    			header_num++;
    			if(header_num == 5){
    				header_num = 0;
    	        	HAL_UART_Receive_DMA(&DYNAUART, (uint8_t*)dy_rxbuff, 2);
    	    		rxpacket_flag = Wait_Length;
    	    		break;
    			}
    		}else{
    			header_num = 0;
    		}
    		HAL_UART_Receive_DMA(&DYNAUART, (uint8_t*)dy_rxbuff, 1);
			rxpacket_flag = Wait_Header;
    		break;
    	case Wait_Length:
    		for(uint8_t i = 0; i < 2; i++)
    			rxpacket[5+i] = dy_rxbuff[i];
    		rxpacket_len = dy_rxbuff[0]|(dy_rxbuff[1]<<8);
        	HAL_UART_Receive_DMA(&DYNAUART, (uint8_t*)dy_rxbuff, (uint8_t)rxpacket_len);
        	rxpacket_flag = Wait_Body;
    		break;
    	case Wait_Body:
    		for(uint8_t i =0; i<rxpacket_len; i++){
    			rxpacket[7+i] = dy_rxbuff[i];
    		}
    		if(rxpacket[7]==0x55){

    			if(received_data != NULL){
        			for(uint8_t i = 0; i < rxpacket_len - 4; i++){
        				received_data[i] = rxpacket[9 + i];
        			}
    			}
    			if(received_err!= NULL)
    				*received_err = rxpacket[8];
    		}
            rxpacket_flag = RxDone;
    		dy_buswait = 0;
            break;
    	case Wait_Tx:
    		HAL_UART_Receive_DMA(&DYNAUART, (uint8_t*)dy_rxbuff, 1);
			rxpacket_flag = Wait_Header;
			break;
    	case RxDone:
    		break;
    	}
    }
}

int8_t InitDynamixel(DynaID id){
  DynaError derr;
  InstWrite_uint8_t(id, Operatinng_Mode, 3, &derr);
  HAL_Delay(5);
  InstWrite_uint8_t(id, Torque_Enable, 1, &derr);
  HAL_Delay(100);
  return 0;
}

int8_t SetPosition(DynaID id, uint16_t deg){
  int32_t pos;
  DynaError derr;
  if(deg >= 360)
    deg =359;
  pos = deg * 4096 /360;
  InstWrite_int32_t(id, Goal_Position, pos, &derr);
  return 0;
}

int8_t SetHandPosition(int8_t dipDeg_fromHome){
  SetPosition(DynaR, RIGHT_HOMEPOS - dipDeg_fromHome);//Larger value -> lift up  230 is parallel
  HAL_Delay(5);
  SetPosition(DynaL, LEFT_HOMEPOS + dipDeg_fromHome);//Less value -> Lift up   139 is parallel
  HAL_Delay(5);
  return 0;
}

*/
