//
// Created by naoki on 19/03/03.
//
#include "main.h"
#include "slave_actuator.h"
#include <stdio.h>
#include <stdbool.h>
uart_com_handler com_handler[UART_COM_MAX_HANDLER];

int l_chika(handler_arg value){
    HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
}

int echo_back(handler_arg value){
    /*
    handler_arg tx_data;
    tx_data.u32_val = value.u32_val+1;
    printf("value:%d\n", tx_data.u32_val);
    uart_com_send(UART_COM_HANDLER_PRINT_U32, tx_data);
	*/
}

int print_u32(handler_arg value){
	/*
    HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
    uint8_t tmp[100] = {0};
    uint16_t len = sprintf(tmp, "value:%d\n", value.u32_val);
    //printf("%d\n", value.u32_val);
     */
}

int receive_command(handler_arg value){
  switch(value.u32_val){
  default:
    break;
  }
  return 0;
}

int receive_gerge_servo(handler_arg value){
  HAL_GPIO_TogglePin(LD2_GPIO_Port,LD2_Pin);
  switch(value.u32_val){
  case UART_GERGESERVO_REDNORMAL:
    SetGergeAngle(Gerge_Angle_Red);
    break;
  case UART_GERGESERVO_REDPASS:
    SetGergeAngle(Gerge_Angle_RedPassing);
    break;
  case UART_GERGESERVO_BLUENORMAL:
    SetGergeAngle(Gerge_Angle_Blue);
    break;
  case UART_GERGESERVO_BLUEPASS:
    SetGergeAngle(Gerge_Angle_BluePassing);
    break;
  default:
    //SetGergeAngle(Gerge_Angle_Passing);
    break;
  }
  return 0;
}

int receive_fire_angle(handler_arg value){
  switch(value.u32_val){
  case UART_FIREANGLE_UP:
    goal_fire_angle = -5;
    break;
  case UART_FIREANGLE_DOWN:
    goal_fire_angle = -100;
  }
  return 0;
}

int receive_rail(handler_arg value){
  switch(value.u32_val){
  case UART_RAIL_CONTRACT:
    goal_rail_pos = RAIL_CONTRACT_LIMIT;
    break;
  case UART_RAIL_EXPAND:
    goal_rail_pos = RAIL_EXPAND_LIMIT;
    break;
  }
}

int receive_Reset(handler_arg value){
  rcvd_state = STATE_RESET;
  if(value.u32_val == 1){
    field_color = RED_FIELD;
  }else{
    field_color = BLUE_FIELD;
  }
  return 0;
}

int receive_Forest(handler_arg value){
  rcvd_state = STATE_FOREST;
  return 0;
}

int receive_AfterBridge(handler_arg value){
  rcvd_state = STATE_AFTER_BRIDGE;
  return 0;
}

int receive_AfterLine(handler_arg value){
  rcvd_state = STATE_AFTER_LINE;
  return 0;
}

int receive_MoveNearShagai(handler_arg value){
  rcvd_state = STATE_MOVE_NEAR_SHAGAI;
  return 0;
}

int receive_MoveBehindShagai(handler_arg value){
  rcvd_state = STATE_MOVE_BEHIND_SHAGAI;
  return 0;
}

int receive_LockShagai(handler_arg value){
  rcvd_state = STATE_LOCK_SHAGAI;
  return 0;
}

int receive_GraspShagai(handler_arg value){
  rcvd_state = STATE_GRASP_SHAGAI;
  return 0;
}

int receive_ElavateShagai(handler_arg value){
  rcvd_state = STATE_ELEVATE_SHAGAI;
  return 0;
}

int receive_ReadyToShoot(handler_arg value){
  rcvd_state = STATE_READY_TO_SHOOT;
  return 0;
}

int receive_ShootShagai(handler_arg value){
  rcvd_state = STATE_SHOOT_SHAGAI;
  return 0;
}

int receive_StorePoker(handler_arg value){
  rcvd_state = STATE_STORE_POKER;
  return 0;
}

int receive_Adjust_FireAngle(handler_arg value){
  static uint32_t last_time;

  if(HAL_GetTick() - last_time >1000){
    last_time = HAL_GetTick();
    if(fire_angle_trapezoid.state ==REST){
      rcvd_fire_angle = (int32_t)value.u32_val;
    }
  }
  return 0;
}

int receive_Adjust_DynaAngle(handler_arg value){
  rcvd_shagai_hand_angle = (int32_t)value.u32_val;
  return 0;
}

int receive_Null(handler_arg value){
  rcvd_state = STATE_NULL;
  return 0;
}

int receive_Freeze(handler_arg value){
  rcvd_state = STATE_FREEZE;
  return 0;
}


int uart_com_handler_init(){

    //Add recv handler
    com_handler[UART_COM_HANDLER_State_AfterBridge] = receive_AfterBridge;
    com_handler[UART_COM_HANDLER_State_AfterLine] = receive_AfterLine;
    com_handler[UART_COM_HANDLER_State_ElavateShagai] = receive_ElavateShagai;
    com_handler[UART_COM_HANDLER_State_Forest] = receive_Forest;
    com_handler[UART_COM_HANDLER_State_GraspShagai] = receive_GraspShagai;
    com_handler[UART_COM_HANDLER_State_LockShagai] = receive_LockShagai;
    com_handler[UART_COM_HANDLER_State_MoveBehindShagai] = receive_MoveBehindShagai;
    com_handler[UART_COM_HANDLER_State_MoveNearShagai] = receive_MoveNearShagai;
    com_handler[UART_COM_HANDLER_State_ReadyToShoot] = receive_ReadyToShoot;
    com_handler[UART_COM_HANDLER_State_Reset] = receive_Reset;
    com_handler[UART_COM_HANDLER_State_ShootShagai] = receive_ShootShagai;
    com_handler[UART_COM_HANDLER_State_StorePoker] = receive_StorePoker;
    com_handler[UART_COM_HANDLER_Adjust_DynaAngle] = receive_Adjust_DynaAngle;
    com_handler[UART_COM_HANDLER_Adjust_FireAngle] = receive_Adjust_FireAngle;
    com_handler[UART_COM_HANDLER_State_Null] = receive_Null;
    com_handler[UART_COM_HANDLER_State_FREEZE] = receive_Freeze;
}

int uart_com_handler_handle(uint8_t tag, handler_arg value){
    uart_com_handler handler = com_handler[tag];
    handler(value);
}
