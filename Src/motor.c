/*
 * motor.c
 *
 *  Created on: 2018/11/25
 *      Author: nabeya11
 */

#include <math.h>
#include <stdlib.h>
#include "main.h"
#include "motor.h"


/*-----------------------------------------------
 * OtherMotor
 ----------------------------------------------*/
/*
 * TIM8�̃��[�^������
 * @param
 * @return
 */
void Generalmotor_init(void){
	HAL_TIM_PWM_Start(&htim8, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim8, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim8, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim8, TIM_CHANNEL_4);
}

/*
 * TIM8�̃��[�^�쓮
 * @param	uint32_t g_channnel �쓮�`�����l��
 * 			float mspeed ���x-1~1
 * @return
 */
void Generalmotor(uint32_t g_channel,float mspeed){
	switch (g_channel) {
		case TIM_CHANNEL_1:
			if(mspeed == 0){
				HAL_GPIO_WritePin(INA5_GPIO_Port,INA5_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(INB5_GPIO_Port,INB5_Pin,GPIO_PIN_RESET);
			}
			else if(mspeed > 0){
				HAL_GPIO_WritePin(INA5_GPIO_Port,INA5_Pin,GPIO_PIN_SET);
				HAL_GPIO_WritePin(INB5_GPIO_Port,INB5_Pin,GPIO_PIN_RESET);
			}
			else{
				HAL_GPIO_WritePin(INA5_GPIO_Port,INA5_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(INB5_GPIO_Port,INB5_Pin,GPIO_PIN_SET);
			}
			break;
		case TIM_CHANNEL_2:
			if(mspeed == 0){
				HAL_GPIO_WritePin(INA6_GPIO_Port,INA6_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(INB6_GPIO_Port,INB6_Pin,GPIO_PIN_RESET);
			}
			else if(mspeed > 0){
				HAL_GPIO_WritePin(INA6_GPIO_Port,INA6_Pin,GPIO_PIN_SET);
				HAL_GPIO_WritePin(INB6_GPIO_Port,INB6_Pin,GPIO_PIN_RESET);
			}
			else{
				HAL_GPIO_WritePin(INA6_GPIO_Port,INA6_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(INB6_GPIO_Port,INB6_Pin,GPIO_PIN_SET);
			}
			break;
		case TIM_CHANNEL_3:
			if(mspeed == 0){
				HAL_GPIO_WritePin(INA7_GPIO_Port,INA7_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(INB7_GPIO_Port,INB7_Pin,GPIO_PIN_RESET);
			}
			else if(mspeed > 0){
				HAL_GPIO_WritePin(INA7_GPIO_Port,INA7_Pin,GPIO_PIN_SET);
				HAL_GPIO_WritePin(INB7_GPIO_Port,INB7_Pin,GPIO_PIN_RESET);
			}
			else{
				HAL_GPIO_WritePin(INA7_GPIO_Port,INA7_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(INB7_GPIO_Port,INB7_Pin,GPIO_PIN_SET);
			}
			break;
		case TIM_CHANNEL_4:
			if(mspeed == 0){
				HAL_GPIO_WritePin(INA8_GPIO_Port,INA8_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(INB8_GPIO_Port,INB8_Pin,GPIO_PIN_RESET);
			}
			else if(mspeed > 0){
				HAL_GPIO_WritePin(INA8_GPIO_Port,INA8_Pin,GPIO_PIN_SET);
				HAL_GPIO_WritePin(INB8_GPIO_Port,INB8_Pin,GPIO_PIN_RESET);
			}
			else{
				HAL_GPIO_WritePin(INA8_GPIO_Port,INA8_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(INB8_GPIO_Port,INB8_Pin,GPIO_PIN_SET);
			}
			break;
		default:
			mspeed=0;
			return;
	}

	uint32_t mspeed_32=fabsf(mspeed*__HAL_TIM_GET_AUTORELOAD(&htim8));
	__HAL_TIM_SET_COMPARE(&htim8, g_channel, mspeed_32);
}


/*-----------------------------------------------
 * Servo
 ----------------------------------------------*/
/*
 * �T�[�{���[�^������
 * @param
 * @return
 */
void Servomotor_init(void){
	HAL_TIM_PWM_Start(&SERVO_1, TIM_CHANNEL_1);
//	HAL_TIM_PWM_Start(&SERVO_2, TIM_CHANNEL_1);
//	HAL_TIM_PWM_Start(&SERVO_3, TIM_CHANNEL_1);
}

/*
 * �T�[�{���[�^�쓮
 * @param	TIM_HandleTypeDef *htims �쓮�^�C�}
 * 			float value �p�x0~1800(�x����*10)
 * @return
 */
void Servomotor(TIM_HandleTypeDef *htims,uint32_t value){
	__HAL_TIM_SET_COMPARE(htims, TIM_CHANNEL_1, value);
}


/*-----------------------------------------------
 * Valve
 ----------------------------------------------*/
/*
 * �d���كI���I�t
 * @param	valve_t valve_num �쓮�d����
 * 			valvestate_t valve_state ON/OFF
 * @return
 */
void Valve(valve_t valve_num, valvestate_t valve_state){
	switch(valve_num){
		case VALVE1:
			HAL_GPIO_WritePin(VALVE1_GPIO_Port,VALVE1_Pin,valve_state);
			break;
		case VALVE2:
			HAL_GPIO_WritePin(VALVE2_GPIO_Port,VALVE2_Pin,valve_state);
			break;
		case VALVE3:
			HAL_GPIO_WritePin(VALVE3_GPIO_Port,VALVE3_Pin,valve_state);
			break;
		default:
			break;
	}
}

/*
 * �d���كg�O��
 * @param	valve_t valve_num �쓮�d����
 * @return
 */
void Valve_Toggle(valve_t valve_num){
	switch(valve_num){
		case VALVE1:
			HAL_GPIO_TogglePin(VALVE1_GPIO_Port,VALVE1_Pin);
			break;
		case VALVE2:
			HAL_GPIO_TogglePin(VALVE2_GPIO_Port,VALVE2_Pin);
			break;
		case VALVE3:
			HAL_GPIO_TogglePin(VALVE3_GPIO_Port,VALVE3_Pin);
			break;
		default:
			break;
	}
}
