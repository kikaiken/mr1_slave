/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dma.h"
#include "gfxsimulator.h"
#include "i2c.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "motor.h"
#include <math.h>
#include <stdio.h>
#include "uart_com.h"
#include "Dynamixel.h"
#include "slave_actuator.h"
#include "dma_printf/dma_printf.h"
#include "dynamixel/dynamixel.h"
#include "dynamixel/dynamixel_com.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
//#define DEBUG_WITHOUT_DYNAMIXEL



/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void PokerTestSetUp();
void PokerTestLoop();
void FireAngleTestSetUp();
void FireAngleTestLoop();
void RailTestSetUp();
void RailTestLoop();



/*
 *
PA5 2_IN5
PA6 2_IN6
PB1 2_IN9

PF10 3_IN8
PF4 3_IN14
PF5 3_IN15
 */
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
  if (htim->Instance == TIM_10MS){
    When10msPassed();
  }
  if(htim->Instance == TIM_PID){            //1ms周期の割り込み。主としてPID制御を行う
    //Process uart_com
    uart_com_timer();
#ifndef DEBUG_WITHOUT_DYNAMIXEL

        dynamixel_com_timer();
#endif

    When1msPassed();
  }
}
#ifdef __GNUC__
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */
int __io_putchar(int ch)
{
  dma_printf_putc(ch&0xFF);
  return ch;
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
  HAL_Delay(1000);
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART3_UART_Init();
  MX_TIM1_Init();
  MX_TIM3_Init();
  MX_TIM2_Init();
  MX_SPI3_Init();
  MX_I2C2_Init();
  MX_TIM6_Init();
  MX_TIM8_Init();
  MX_TIM4_Init();
  MX_TIM7_Init();
  MX_TIM12_Init();
  MX_ADC3_Init();
  MX_ADC2_Init();
  MX_TIM14_Init();
  MX_USART2_UART_Init();
  MX_GFXSIMULATOR_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */

  //Initialize DMA printf
  setbuf(stdin, NULL);
  setbuf(stdout, NULL);
  setbuf(stderr, NULL);
  dma_printf_init(&huart3);

  //Initialize uart_com
  uart_com_init(&huart1);



  HAL_TIM_Base_Start_IT(&TIM_10MS_HANDLER);
  HAL_TIM_Base_Start_IT(&TIM_PID_HANDLER);

  log_info("Hello MR1 Slave");

  HAL_GPIO_WritePin(LD2_GPIO_Port,LD2_Pin, GPIO_PIN_SET);
  EnablePotentio();
  printf("potentil");
  HAL_GPIO_WritePin(LD2_GPIO_Port,LD2_Pin, GPIO_PIN_RESET);
  //HAL_ADC_Start_DMA(&hadc3, (uint32_t *)adc3_val, ADC_CONVERTED_DATA_BUFFER_SIZE);
  Generalmotor_init();
  Servomotor_init();
  EnableFireAngleEncoder();
  EnableRailEncoder();
  HAL_Delay(1000);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  //モータに出力が行われないステートに設定
  state.attitude = STATE_NULL;
  state.is_stable = 1;

  //位置制御の初期化
  InitRailTrapezoid();
  InitPokerTrapezoid();
  InitFireAngleTrapezoid();


#ifndef DEBUG_WITHOUT_DYNAMIXEL
  dynamixel_init(&huart2);
#endif
  while (1)
  {
    //受信した姿勢命令に応じて目標姿勢を変更する。ただしSTATE_NULL、STATE_FREEZEは優先的に処理する
    if(rcvd_state != state.attitude){
      if(state.attitude == STATE_NULL){//現在ステートがエラー系統の時は例外的な処理
        if(rcvd_state == STATE_RESET){
          TransitState(rcvd_state);
        }
      }else if(rcvd_state == STATE_NULL || rcvd_state == STATE_FREEZE){//STATE_NULL、STATE_FREEZEは優先的に処理する
        TransitState(rcvd_state);
      }else if(state.is_stable == 1){
        if(state.attitude == STATE_FREEZE){//フリーズから射角にかかわるステートには移動させない
          if(rcvd_state <= STATE_MOVE_BEHIND_SHAGAI){
            TransitState(rcvd_state);
          }
        }else {
          AttitudeState set_state = rcvd_state;
          if(set_state >= STATE_MOVE_NEAR_SHAGAI && set_state - state.attitude > 1){
            set_state = state.attitude + 1;
          }
          TransitState(set_state);
        }
      }
    }
    HAL_Delay(10);
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Configure LSE Drive Capability 
  */
  HAL_PWR_EnableBkUpAccess();
  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 216;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 6;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Activate the Over-Drive mode 
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_USART2
                              |RCC_PERIPHCLK_USART3|RCC_PERIPHCLK_I2C2;
  PeriphClkInitStruct.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInitStruct.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInitStruct.Usart3ClockSelection = RCC_USART3CLKSOURCE_PCLK1;
  PeriphClkInitStruct.I2c2ClockSelection = RCC_I2C2CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void FireAngleTestSetUp(){
  EnableFireAngleEncoder();
  ResetHomeFireAngle();
}

void FireAngleTestLoop(){
  GetCurrentFireAngle();
  UpdateShagaiFireAngle(goal_fire_angle);
  StorePastFireAngle();
}

void RailTestSetUp(){

}

void RailTestLoop(){//-1485 shrink   -1035 expand
  GetCurrentRailPos();
  UpdateRail(goal_rail_pos);
  StorePastRailPos();
}

void PokerTestSetUp(){

}

void PokerTestLoop(){
  GetCurrentPokerPos();
  UpdatePoker(goal_poker_pos);
  StorePastPokerPos();
}

//void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc){
//  if(hadc == &hadc2){
//
//  }
//  if(hadc == &hadc3){
//
//  }
//}


void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){

  if( GPIO_Pin == FIREANGLE_LIMSW_PIN ){
    WhenFireLimitSWPushed();
  }

  if(GPIO_Pin == RAIL_LIMSW_PIN){
    WhenRailLimitSWPushed();
  }
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
