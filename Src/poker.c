/*
 * poker.c
 *
 *  Created on: 2019/03/24
 *      Author: Ryohei
 */

#include "poker.h"
#include "math.h"
#include "motor.h"
#include "constant.h"

void SetPokerPos(float ref_pos, float current_pos, float past_pos){
  static float sum_of_err = 0;
  float output;

//  sum_of_err += fmax(fmin((ref_pos - current_pos), 1.0), -1.0);

  output = PID(ref_pos, current_pos, past_pos, CTRL_CYCLE_MS, 0, 0, 0, &sum_of_err, poker_Ki_term_limit);
  Generalmotor(POKER_PWM_CH, output);
}
