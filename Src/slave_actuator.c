/*
 * slave_actuator.c
 *
 *  Created on: 2019/03/26
 *      Author: Eater
 */

#include "slave_actuator.h"

#define BOUND(x, max_val, min_val)  fmax(fmin(x, max_val), min_val)

//#define SIGN(x) (x>0 ? 1 : (x<0 ? -1 : 0))
static inline  float SIGN(float x){
  if(x >= 0)
    return 1;
  else
    return -1;
}

SlaveState volatile state;
FieldColor field_color =0;
volatile AttitudeState rcvd_state = STATE_NULL;

float poker_max_acceleration = 5000;
float poker_max_velocity = 1000;

float rail_max_acceleration = 5000;
float rail_max_velocity = 400;

float fire_angle_max_acceleration = 200;
float fire_angle_max_velocity = 500;

volatile uint16_t adc2_val[3];
volatile uint16_t adc3_val[1];
float potentiometer[6];
float current_fire_angle;
volatile int32_t set_fire_angle = FIRE_ANGLE_FIRE;
float current_rail_pos;
uint32_t current_rail_potentio;
float current_poker_pos;
uint16_t current_gerge_angle;
float current_shagai_hand_angle;
volatile int32_t set_shagai_hand_angle = DYNAMIXEL_ANGLE_PARALLEL_SHOOT;
float past_fire_angle;
float past_rail_pos;
float past_poker_pos;
float past_shagai_hand_angle;

float pastpast_fire_angle;
float pastpast_rail_pos;
float pastpast_poker_pos;
uint16_t pastpast_gerge_angle;
float pastpast_shagai_hand_angle;

volatile int32_t rcvd_shagai_hand_angle = 0;
volatile int32_t rcvd_fire_angle = 0;
float rcvd_rail_pos;
float rcvd_poker_pos;

float output_fire_angle;
float fire_angle_Kp = 0.05;
float fire_angle_Ki = 0.0;
float fire_angle_Kd = 0.0;
volatile float goal_fire_angle = -5;
float fire_angle_fail = 20;
float output_rail_pos;
float rail_pos_Kp = 0.011;
float rail_pos_Ki = 0.0;//6.0E-10;
float rail_pos_Kd = 0.0;
volatile float goal_rail_pos = RAIL_CONTRACT_LIMIT;
float rail_pos_fail = 200;
float output_poker_pos;
float poker_pos_Kp = 0.008;
float poker_pos_Ki = 0.0;
float poker_pos_Kd = 0.0;
float poker_Ki_term_limit = 0.1;
float rail_Ki_term_limit = 0.1;
float fire_angle_Ki_term_limit = 0.1;

volatile float goal_poker_pos = POKER_CONTRACT_LIMIT;
float poker_pos_fail = -200;
uint16_t gerge_angle;
uint8_t lim3val;

uint32_t fire_angle_encoder;

int8_t is_fire_angle_set = 0;
int8_t is_fire_angle_limited = 0;
int8_t is_rail_pos_set = 0;
int8_t is_rail_pos_limited = 0;
int8_t is_poker_pos_limited = 0;

volatile float freeze_rail_pos;
volatile float freeze_poker_pos;
volatile float freeze_fire_angle_pos;

volatile TrapezoidVariable poker_trapezoid;
volatile TrapezoidVariable rail_trapezoid;
volatile TrapezoidVariable fire_angle_trapezoid;
volatile int8_t is_initialized = 0;

volatile uint32_t rail_offset_potentio;

volatile int8_t release_msec_offset = 0;

#define VAL_a (208)
#define VAL_b (265.892)
#define VAL_h (106)
#define VAL_l (273)
#define MAX_LENGTH_FROM_FULCRUM (1350)
#define RAIL_DEFAULT_LENGTH (460)
#define CYLINDER_STROKE (455)

float rail_len;

int8_t is_stable = 0;

volatile int8_t is_throwing = 0;

volatile float now ;//debug用

void When10msPassed(){
  static int time = 0;

  log_info("rail_pos: %f", current_rail_pos);

  time++;

  if(time > 10){
    time = 0;
    CheckFireTimingButton();
    DisplayOnLED((uint8_t)release_msec_offset);
  }
}

void When1msPassed(){
  //モータの現在位置を取得
  GetCurrentPokerPos();
  GetCurrentRailPos();
  GetCurrentFireAngle();

  //今指定されている姿勢と現在姿勢が一致しているかを確認
  CheckAttitude();

  //位置制御の初期化が完了する前にモータを駆動させないようにする
  if(is_initialized == 1){
    //現在の目標位置に対するPID制御
    UpdatePokerGoalPos();
    UpdateRailGoalPos();
    UpdateFireAngleGoalPos();
  }else{
    PokerMotor(0);
    RailMotor(0);
    FireAngleMotor(0);
  }

  //モータの現在位置を保存
  StorePastPokerPos();
  StorePastRailPos();
  StorePastFireAngle();

}

void WhenRailLimitSWPushed(){
  if(HAL_GPIO_ReadPin(RAIL_LIMSW_PORT, RAIL_LIMSW_PIN) == GPIO_PIN_SET){
    if(is_throwing == 0){
      is_rail_pos_limited = 1;
      TIM_RAIL_ENC->CNT = RAIL_ENC_RESETVAL;
      RailMotor(0);
    }
    log_info("rail limited");
  }

}

void WhenFireLimitSWPushed(){
  if(HAL_GPIO_ReadPin(FIREANGLE_LIMSW_PORT,FIREANGLE_LIMSW_PIN) == GPIO_PIN_RESET){
    TIM_FIREANGLE_ENC->CNT = FIREANGLE_ENC_RESETVAL;
    is_fire_angle_limited = 1;
    FireAngleMotor(0);
  }
}

int8_t State_Delay(uint32_t time){//0正常終了, 1:緊急なステート変更
  for(uint32_t t = 0; t < time; t++){
    if(rcvd_state != state.attitude && (rcvd_state == STATE_NULL || rcvd_state == STATE_FREEZE)){
      is_initialized = 0;
      FireAngleMotor(0);
      RailMotor(0);
      PokerMotor(0);

      return 1;
    }
    HAL_Delay(1);
  }
  return 0;//正常終了
}

float RadianFrom_fire_angle_val(float angle_val){
  float theta;
  float phi;
  angle_val = fabs(angle_val);
  if(angle_val + VAL_l == 0){
    theta = M_PI / 2.0;
  }else{
     theta = atan(VAL_h / (angle_val + VAL_l));
  }

  float y = sqrt(pow((angle_val + VAL_l),2) + pow(VAL_h,2));

  if((2*y*VAL_b) == 0){
    phi = 0;
  }else{
    phi = acos((pow(y,2) + pow(VAL_b,2) - pow(VAL_a,2))/(2*y*VAL_b));
  }

  return phi - theta;
}

float RailLengthFromFireAngle(float rad){
  if(cos(rad) == 0){
    return RAIL_EXPAND_LIMIT;
  }
  rail_len = MAX_LENGTH_FROM_FULCRUM / cos(rad);
  return (float)(fmin((MAX_LENGTH_FROM_FULCRUM / cos(rad)) - 50 - RAIL_DEFAULT_LENGTH - CYLINDER_STROKE, RAIL_EXPAND_LIMIT));
}

void TransitState(AttitudeState attitude){

  if(attitude == state.attitude)
    return;

  if(rcvd_fire_angle != 0)
    set_fire_angle = fmax(fmin(rcvd_fire_angle, FIRE_ANGLE_UP), FIRE_ANGLE_DOWN);

  if(rcvd_shagai_hand_angle != 0)
    set_shagai_hand_angle = rcvd_shagai_hand_angle;

  switch(attitude){
  case STATE_RESET:
    state.attitude = attitude;
    state.is_stable = 0;

	ShagaiHand(0);
    InitFireAngleTrapezoid();
    InitPokerTrapezoid();
    InitRailTrapezoid();
    is_initialized = 1;
    if(State_Delay(500) == 1){
      break;
    }
    if(field_color == RED_FIELD){
      SetGergeAngle(Gerge_Angle_Red);
    }else{
      SetGergeAngle(Gerge_Angle_Blue);
    }
    ResetHomeFireAngle();
    if(State_Delay(100) == 1){
      break;
    }

    ResetHomeRail();
    if(State_Delay(100) == 1){
      break;
    }
    InitFireAngleTrapezoid();
    InitRailTrapezoid();
    SetRailGoalPos(RAIL_CONTRACT_LIMIT);
    SetFireAngleGoalPos(FIRE_ANGLE_UP);
    SetPokerGoalPos(POKER_CONTRACT_LIMIT);
    break;
  case STATE_FOREST:
    state.attitude = attitude;
    state.is_stable = 0;

    if(field_color == RED_FIELD){
      SetGergeAngle(Gerge_Angle_Red);
    }else{
      SetGergeAngle(Gerge_Angle_Blue);
    }
    SetRailGoalPos(RAIL_CONTRACT_LIMIT);
    SetFireAngleGoalPos(FIRE_ANGLE_UP);
    SetPokerGoalPos(POKER_CONTRACT_LIMIT);
    break;
  case STATE_AFTER_BRIDGE:
    state.attitude = attitude;
    state.is_stable = 0;

    if(field_color == RED_FIELD){
      SetGergeAngle(Gerge_Angle_RedPassing);
    }else{
      SetGergeAngle(Gerge_Angle_BluePassing);
    }
    SetRailGoalPos(RAIL_CONTRACT_LIMIT);
    SetFireAngleGoalPos(FIRE_ANGLE_UP);
    SetPokerGoalPos(POKER_CONTRACT_LIMIT);
    break;
  case STATE_AFTER_LINE:
    state.attitude = attitude;
    state.is_stable = 0;

    if(field_color == RED_FIELD){
      SetGergeAngle(Gerge_Angle_RedPassing);
    }else{
      SetGergeAngle(Gerge_Angle_BluePassing);
    }
    SetRailGoalPos(RAIL_CONTRACT_LIMIT);
    SetFireAngleGoalPos(FIRE_ANGLE_UP);
    SetPokerGoalPos(POKER_CONTRACT_LIMIT);
    break;
  case STATE_MOVE_NEAR_SHAGAI:
    state.attitude = attitude;
    state.is_stable = 0;

    ShagaiHand(0);

    if(field_color == RED_FIELD){
      SetGergeAngle(Gerge_Angle_Red);
    }else{
      SetGergeAngle(Gerge_Angle_Blue);
    }

    SetRailGoalPos(RAIL_CONTRACT_LIMIT);
    SetFireAngleGoalPos(FIRE_ANGLE_UP);
    SetPokerGoalPos(POKER_CONTRACT_LIMIT);
	dynamixel_set_pos_all(DYNAMIXEL_ANGLE_PARALLEL_WITH_FIREANGLE);
    break;
  case STATE_MOVE_BEHIND_SHAGAI:
    state.attitude = attitude;
    state.is_stable = 0;

    ShagaiHand(0);

    if(field_color == RED_FIELD){
      SetGergeAngle(Gerge_Angle_Red);
    }else{
      SetGergeAngle(Gerge_Angle_Blue);
    }

    SetRailGoalPos(RAIL_CONTRACT_LIMIT);
    SetFireAngleGoalPos(FIRE_ANGLE_UP);
    SetPokerGoalPos(POKER_EXPAND_LIMIT);
    dynamixel_set_pos_all(DYNAMIXEL_ANGLE_PARALLEL_WITH_FIREANGLE);
    break;
  case STATE_LOCK_SHAGAI:
    state.attitude = attitude;
    state.is_stable = 0;

    ShagaiHand(0);

    if(field_color == RED_FIELD){
      SetGergeAngle(Gerge_Angle_Red);
    }else{
      SetGergeAngle(Gerge_Angle_Blue);
    }

    SetRailGoalPos(RAIL_CONTRACT_LIMIT);
    SetFireAngleGoalPos(FIRE_ANGLE_UP);
    SetPokerGoalPos(POKER_PICK);
    dynamixel_set_pos_all(DYNAMIXEL_ANGLE_PARALLEL_WITH_FIREANGLE);
    break;
  case STATE_GRASP_SHAGAI:
    state.attitude = attitude;
    state.is_stable = 0;

    ShagaiHand(0);


    if(field_color == RED_FIELD){
      SetGergeAngle(Gerge_Angle_Red);
    }else{
      SetGergeAngle(Gerge_Angle_Blue);
    }

    SetFireAngleGoalPos(FIRE_ANGLE_DOWN);
    SetPokerGoalPos(POKER_PICK);
    dynamixel_set_pos_all(DYNAMIXEL_ANGLE_PARALLEL_WITH_FIREANGLE);
    while(fire_angle_trapezoid.current_goal_val != FIRE_ANGLE_DOWN || fire_angle_trapezoid.state != REST){
      if(State_Delay(1) == 1){
        return;
      }
    }
    if(State_Delay(50) == 1){
      break;
    }
    SetRailGoalPos(RAIL_PICK);

    break;
  case STATE_ELEVATE_SHAGAI:
    state.attitude = attitude;
    state.is_stable = 0;

	ShagaiHand(1);
	if(State_Delay(50) == 1){
      break;
    }
	SetPokerGoalPos(POKER_EXPAND_LIMIT);
    dynamixel_set_pos_all(DYNAMIXEL_ANGLE_PARALLEL_WITH_FIREANGLE);
    while(poker_trapezoid.current_goal_val != POKER_EXPAND_LIMIT || poker_trapezoid.state != REST){
      if(State_Delay(1) == 1){
        return;
       }
    }
    if(State_Delay(50) == 1){
      break;
    }



    if(field_color == RED_FIELD){
      SetGergeAngle(Gerge_Angle_Red);
    }else{
      SetGergeAngle(Gerge_Angle_Blue);
    }

    SetRailGoalPos(RAIL_ELEVATE);
    while(current_rail_pos < RAIL_ELEVATE - 10){
      if(State_Delay(1) == 1){
        return;
      }
    }
    SetFireAngleGoalPos(set_fire_angle);

    while(current_fire_angle < FIRE_ANGLE_DOWN + 80){
      if(State_Delay(1) == 1){
        return;
      }
    }
    SetPokerGoalPos(POKER_CONTRACT_LIMIT);
    break;
  case STATE_STORE_POKER:
    state.attitude = attitude;
    state.is_stable = 0;

    ShagaiHand(1);



    if(field_color == RED_FIELD){
      SetGergeAngle(Gerge_Angle_Red);
    }else{
      SetGergeAngle(Gerge_Angle_Blue);
    }

    SetRailGoalPos(RAIL_ELEVATE);
    SetFireAngleGoalPos(set_fire_angle);
    while(current_fire_angle < FIRE_ANGLE_DOWN + 80){
      if(State_Delay(1) == 1){
        return;
      }
    }
    SetPokerGoalPos(POKER_CONTRACT_LIMIT);
    break;
  case STATE_READY_TO_SHOOT:
    state.attitude = attitude;
    state.is_stable = 0;

    ShagaiHand(1);


    if(field_color == RED_FIELD){
      SetGergeAngle(Gerge_Angle_Red);
    }else{
      SetGergeAngle(Gerge_Angle_Blue);
    }

    SetRailGoalPos(RailLengthFromFireAngle(RadianFrom_fire_angle_val(set_fire_angle)));
    SetFireAngleGoalPos(set_fire_angle);
    SetPokerGoalPos(POKER_CONTRACT_LIMIT);
    dynamixel_set_pos_all(set_shagai_hand_angle);
    break;
  case STATE_SHOOT_SHAGAI:
    state.attitude = attitude;
    state.is_stable = 0;

    ShagaiHand(1);
    uint32_t start = HAL_GetTick();

    while(fabsf(current_rail_pos - RailLengthFromFireAngle(RadianFrom_fire_angle_val(set_fire_angle))) > 10.0 || rail_trapezoid.state != REST){
      if(HAL_GetTick() - start > 5000){
        return;
      }
      if(State_Delay(1) == 1){
        return;
      }
    }
    while(fabsf(current_fire_angle - set_fire_angle) > 1.0 || fire_angle_trapezoid.state != REST){
      if(HAL_GetTick() - start > 5000){
        return;
      }
      if(State_Delay(1) == 1){
        return;
      }
    }
    ThrowShagai();
    if(State_Delay(200) == 1){
      break;
    }
    SetRailGoalPos(RAIL_CONTRACT_LIMIT);
    SetFireAngleGoalPos(FIRE_ANGLE_UP);
    while(rail_trapezoid.current_goal_val != RAIL_CONTRACT_LIMIT || rail_trapezoid.state != REST){
      if(State_Delay(1) == 1){
        return;
       }
    }
    ResetHomeRail();
    InitFireAngleTrapezoid();
    if(State_Delay(100) == 1){
      break;
    }
    SetRailGoalPos(RAIL_CONTRACT_LIMIT);
    dynamixel_set_pos_all(DYNAMIXEL_ANGLE_PARALLEL_WITH_FIREANGLE);
    break;
  case STATE_NULL:
    state.attitude = attitude;
    is_initialized = 0;
    break;
  case STATE_FREEZE:
    state.attitude = attitude;
    is_initialized = 0;

    FireAngleMotor(0);
    PokerMotor(0);
    RailMotor(0);

    HAL_Delay(500);

    freeze_fire_angle_pos = current_fire_angle;
    freeze_poker_pos = current_poker_pos;
    freeze_rail_pos = current_rail_pos;

    InitFireAngleTrapezoid();
    InitPokerTrapezoid();
    InitRailTrapezoid();
    is_initialized = 1;
    SetRailGoalPos(freeze_rail_pos);
    SetFireAngleGoalPos(freeze_fire_angle_pos);
    SetPokerGoalPos(freeze_poker_pos);
    break;
  default:
    break;

  }
}

void CheckAttitude(){
  int8_t is_stable = 1;
  int8_t is_shagai_hand_changed = 0;

  if(rcvd_fire_angle != 0)
      set_fire_angle = fmax(fmin(rcvd_fire_angle, FIRE_ANGLE_UP), FIRE_ANGLE_DOWN);
  if(rcvd_shagai_hand_angle != 0 && set_shagai_hand_angle != rcvd_shagai_hand_angle){
      set_shagai_hand_angle = rcvd_shagai_hand_angle;
      is_shagai_hand_changed = 1;
  }

  if(rcvd_state != state.attitude && (rcvd_state == STATE_NULL || rcvd_state == STATE_FREEZE)){
    is_initialized = 0;
    FireAngleMotor(0);
    RailMotor(0);
    PokerMotor(0);
  }

  switch(state.attitude){
  case STATE_RESET:
    if(is_rail_pos_set != 1)
      is_stable = 0;
    if(is_fire_angle_set != 1)
      is_stable = 0;
    if(is_initialized != 1)
      is_stable = 0;

    if(rail_trapezoid.current_goal_val != RAIL_CONTRACT_LIMIT || rail_trapezoid.state != REST)
      is_stable = 0;

    if(fire_angle_trapezoid.current_goal_val != FIRE_ANGLE_UP || fire_angle_trapezoid.state != REST)
      is_stable = 0;

    if(poker_trapezoid.current_goal_val != POKER_CONTRACT_LIMIT || poker_trapezoid.state != REST)
      is_stable = 0;

    state.is_stable = is_stable;
    break;
  case STATE_FOREST:
    if(is_rail_pos_set != 1)
      is_stable = 0;
    if(is_fire_angle_set != 1)
      is_stable = 0;
    if(is_initialized != 1)
      is_stable = 0;

    if(rail_trapezoid.current_goal_val != RAIL_CONTRACT_LIMIT || rail_trapezoid.state != REST)
      is_stable = 0;

    if(fire_angle_trapezoid.current_goal_val != FIRE_ANGLE_UP || fire_angle_trapezoid.state != REST)
      is_stable = 0;

    if(poker_trapezoid.current_goal_val != POKER_CONTRACT_LIMIT || poker_trapezoid.state != REST)
      is_stable = 0;

    state.is_stable = is_stable;

    break;
  case STATE_AFTER_BRIDGE:
    if(is_rail_pos_set != 1)
      is_stable = 0;
    if(is_fire_angle_set != 1)
      is_stable = 0;
    if(is_initialized != 1)
      is_stable = 0;

    if(rail_trapezoid.current_goal_val != RAIL_CONTRACT_LIMIT || rail_trapezoid.state != REST)
      is_stable = 0;

    if(fire_angle_trapezoid.current_goal_val != FIRE_ANGLE_UP || fire_angle_trapezoid.state != REST)
      is_stable = 0;

    if(poker_trapezoid.current_goal_val != POKER_CONTRACT_LIMIT || poker_trapezoid.state != REST)
      is_stable = 0;

    state.is_stable = is_stable;

    break;
  case STATE_AFTER_LINE:
    if(is_rail_pos_set != 1)
      is_stable = 0;
    if(is_fire_angle_set != 1)
      is_stable = 0;
    if(is_initialized != 1)
      is_stable = 0;

    if(rail_trapezoid.current_goal_val != RAIL_CONTRACT_LIMIT || rail_trapezoid.state != REST)
      is_stable = 0;

    if(fire_angle_trapezoid.current_goal_val != FIRE_ANGLE_UP || fire_angle_trapezoid.state != REST)
      is_stable = 0;

    if(poker_trapezoid.current_goal_val != POKER_CONTRACT_LIMIT || poker_trapezoid.state != REST)
      is_stable = 0;

    state.is_stable = is_stable;

    break;
  case STATE_MOVE_NEAR_SHAGAI:
    if(is_rail_pos_set != 1)
      is_stable = 0;
    if(is_fire_angle_set != 1)
      is_stable = 0;
    if(is_initialized != 1)
      is_stable = 0;

    if(rail_trapezoid.current_goal_val != RAIL_CONTRACT_LIMIT || rail_trapezoid.state != REST)
      is_stable = 0;

    if(fire_angle_trapezoid.current_goal_val != FIRE_ANGLE_UP || fire_angle_trapezoid.state != REST)
      is_stable = 0;

    if(poker_trapezoid.current_goal_val != POKER_CONTRACT_LIMIT || poker_trapezoid.state != REST)
      is_stable = 0;

    state.is_stable = is_stable;

    break;
  case STATE_MOVE_BEHIND_SHAGAI:
    if(is_rail_pos_set != 1)
      is_stable = 0;
    if(is_fire_angle_set != 1)
      is_stable = 0;
    if(is_initialized != 1)
      is_stable = 0;

    if(rail_trapezoid.current_goal_val != RAIL_CONTRACT_LIMIT || rail_trapezoid.state != REST)
      is_stable = 0;

    if(fire_angle_trapezoid.current_goal_val != FIRE_ANGLE_UP || fire_angle_trapezoid.state != REST)
      is_stable = 0;

    if(poker_trapezoid.current_goal_val != POKER_EXPAND_LIMIT || poker_trapezoid.state != REST)
      is_stable = 0;

    state.is_stable = is_stable;

    break;
  case STATE_LOCK_SHAGAI:
    if(is_rail_pos_set != 1)
      is_stable = 0;
    if(is_fire_angle_set != 1)
      is_stable = 0;
    if(is_initialized != 1)
      is_stable = 0;

    if(rail_trapezoid.current_goal_val != RAIL_CONTRACT_LIMIT || rail_trapezoid.state != REST)
      is_stable = 0;

    if(fire_angle_trapezoid.current_goal_val != FIRE_ANGLE_UP || fire_angle_trapezoid.state != REST)
      is_stable = 0;

    if(poker_trapezoid.current_goal_val != POKER_PICK || poker_trapezoid.state != REST)
      is_stable = 0;

    state.is_stable = is_stable;

    break;
  case STATE_GRASP_SHAGAI:
    if(is_rail_pos_set != 1)
      is_stable = 0;
    if(is_fire_angle_set != 1)
      is_stable = 0;
    if(is_initialized != 1)
      is_stable = 0;

    if(rail_trapezoid.current_goal_val != RAIL_PICK || rail_trapezoid.state != REST)
      is_stable = 0;

    if(fire_angle_trapezoid.current_goal_val != FIRE_ANGLE_DOWN || fire_angle_trapezoid.state != REST)
      is_stable = 0;

    if(poker_trapezoid.current_goal_val != POKER_PICK || poker_trapezoid.state != REST)
      is_stable = 0;

    state.is_stable = is_stable;

    break;
  case STATE_ELEVATE_SHAGAI:
    if(is_rail_pos_set != 1)
      is_stable = 0;
    if(is_fire_angle_set != 1)
      is_stable = 0;
    if(is_initialized != 1)
      is_stable = 0;

    if(set_fire_angle != fire_angle_trapezoid.post_goalpos){
      is_stable = 0;
      if(current_rail_pos > RAIL_ELEVATE-10)
        SetFireAngleGoalPos(set_fire_angle);
    }

    if(rail_trapezoid.current_goal_val != RAIL_ELEVATE || rail_trapezoid.state != REST)
      is_stable = 0;

    if(fire_angle_trapezoid.current_goal_val != set_fire_angle || fire_angle_trapezoid.state != REST)
      is_stable = 0;

    if(poker_trapezoid.current_goal_val != POKER_CONTRACT_LIMIT || poker_trapezoid.state != REST)
      is_stable = 0;

    state.is_stable = is_stable;

    break;
  case STATE_STORE_POKER:
    if(is_rail_pos_set != 1)
      is_stable = 0;
    if(is_fire_angle_set != 1)
      is_stable = 0;
    if(is_initialized != 1)
      is_stable = 0;

    if(set_fire_angle != fire_angle_trapezoid.post_goalpos){
      is_stable = 0;
      SetFireAngleGoalPos(set_fire_angle);
    }

    if(rail_trapezoid.current_goal_val != RAIL_ELEVATE || rail_trapezoid.state != REST)
      is_stable = 0;

    if(fire_angle_trapezoid.current_goal_val != set_fire_angle || fire_angle_trapezoid.state != REST)
      is_stable = 0;

    if(poker_trapezoid.current_goal_val != POKER_CONTRACT_LIMIT || poker_trapezoid.state != REST)
      is_stable = 0;

    state.is_stable = is_stable;
    break;
  case STATE_READY_TO_SHOOT:
    if(is_rail_pos_set != 1)
      is_stable = 0;
    if(is_fire_angle_set != 1)
      is_stable = 0;
    if(is_initialized != 1)
      is_stable = 0;

    if(is_shagai_hand_changed == 1)
      dynamixel_set_pos_all(set_shagai_hand_angle);

    if(set_fire_angle != fire_angle_trapezoid.post_goalpos){
      is_stable = 0;
      SetFireAngleGoalPos(set_fire_angle);
      SetRailGoalPos(RailLengthFromFireAngle(RadianFrom_fire_angle_val(set_fire_angle)));
    }

    if(rail_trapezoid.current_goal_val != (float)RailLengthFromFireAngle(RadianFrom_fire_angle_val(set_fire_angle)) || rail_trapezoid.state != REST)
      is_stable = 0;

    if(fire_angle_trapezoid.current_goal_val != set_fire_angle || fire_angle_trapezoid.state != REST)
      is_stable = 0;

    if(poker_trapezoid.current_goal_val != POKER_CONTRACT_LIMIT || poker_trapezoid.state != REST)
      is_stable = 0;

    state.is_stable = is_stable;

    break;
  case STATE_SHOOT_SHAGAI:
    if(is_fire_angle_set != 1)
      is_stable = 0;
    if(is_initialized != 1)
      is_stable = 0;

    if(rail_trapezoid.current_goal_val != RAIL_CONTRACT_LIMIT || rail_trapezoid.state != REST)
      is_stable = 0;

    state.is_stable = is_stable;
    break;
  case STATE_NULL:
    state.is_stable = 1;
    break;
  case STATE_FREEZE:
    if(is_fire_angle_set != 1)
      is_stable = 0;
    if(is_initialized != 1)
      is_stable = 0;

    if(fire_angle_trapezoid.current_goal_val != freeze_fire_angle_pos || fire_angle_trapezoid.state != REST)
      is_stable = 0;

    if(rail_trapezoid.current_goal_val != freeze_rail_pos || rail_trapezoid.state != REST)
      is_stable = 0;

    if(poker_trapezoid.current_goal_val != freeze_poker_pos || poker_trapezoid.state != REST)
      is_stable = 0;

    state.is_stable = is_stable;
    break;
  default:
    break;
  }
}

void ResetHomeRail(){
  is_rail_pos_set = 0;

  uint32_t time = HAL_GetTick();
  while(HAL_GPIO_ReadPin(RAIL_LIMSW_PORT,RAIL_LIMSW_PIN) == GPIO_PIN_SET){
    RailMotor(-0.3);
    if(HAL_GetTick() - time > 10000){
      break;
    }
  }
  RailMotor(0);
  while(HAL_GPIO_ReadPin(RAIL_LIMSW_PORT,RAIL_LIMSW_PIN) == GPIO_PIN_RESET){
    RailMotor(0.3);
  }
  RailMotor(0);
  HAL_Delay(100);
  InitRailTrapezoid();
  is_rail_pos_set = 1;
}

void ResetHomeFireAngle(){// horizontal at -107
  is_fire_angle_set = 0;

  uint32_t time = HAL_GetTick();
  while(HAL_GPIO_ReadPin(FIREANGLE_LIMSW_PORT,FIREANGLE_LIMSW_PIN) == GPIO_PIN_RESET){
    FireAngleMotor(-0.2);
    if(HAL_GetTick() - time > 500){
      break;
    }
  }
  FireAngleMotor(0);
  while(HAL_GPIO_ReadPin(FIREANGLE_LIMSW_PORT,FIREANGLE_LIMSW_PIN) == GPIO_PIN_SET){
    FireAngleMotor(0.2);
  }
  FireAngleMotor(0);
  HAL_Delay(100);
  InitFireAngleTrapezoid();
  is_fire_angle_set = 1;
}

void EnableFireAngleEncoder(){
  HAL_TIM_Encoder_Start(&TIM_FIREANGLE, TIM_CHANNEL_ALL);
  TIM_FIREANGLE_ENC->CNT = FIREANGLE_ENC_RESETVAL;
}

void EnableRailEncoder(){
  HAL_TIM_Encoder_Start(&TIM_RAIL, TIM_CHANNEL_ALL);
  TIM_RAIL_ENC->CNT = RAIL_ENC_RESETVAL;
}

void GetCurrentFireAngle(){
  fire_angle_encoder = TIM_FIREANGLE_ENC->CNT;
  current_fire_angle = ((float)((float)fire_angle_encoder - FIREANGLE_ENC_RESETVAL) * (DIR_OF_FIRE_ANGLE_ENCODER)/(4096.0*((float)NUM_OF_TEETH_OF_FIRE_ANGLE_BELT/(float)NUM_OF_TEETH_OF_FIRE_ANGLE_ENCODER))) * 2*M_PI*FIREANGLE_ENC_RADIUS;//4096で1回転
}

void EnablePotentio(){
  printf("Enable potentio start");
  HAL_ADC_Start_DMA(&hadc2, (uint32_t *)adc2_val, ADC_CONVERTED_DATA_BUFFER_SIZE);
  HAL_Delay(10);
 // HAL_ADC_Start_DMA(&hadc3, (uint32_t *)adc3_val, 1);
  printf("Enable potentio suceeded\n\r");
}

void StorePastFireAngle(){
  pastpast_fire_angle = past_fire_angle;
  past_fire_angle = current_fire_angle;
}

void GetCurrentRailPos(){

  current_rail_potentio = TIM_RAIL_ENC->CNT;
  current_rail_pos = -1 * ((float)((float)current_rail_potentio - RAIL_ENC_RESETVAL)/4096.0) * 2.0*M_PI*RAIL_ENC_RADIUS;//4096で1回転

}

void RailMotor(float mspeed){
  is_rail_pos_limited = 0;

  if(mspeed > 0.9){
    mspeed = 0.9;
  }
  if(mspeed < -0.9){
    mspeed = -0.9;
  }

  if(HAL_GPIO_ReadPin(RAIL_LIMSW_PORT, RAIL_LIMSW_PIN) == GPIO_PIN_SET && mspeed > 0){
    is_rail_pos_limited = 1;
  }

  if(is_rail_pos_set == 1){
    if(current_rail_pos < RAIL_CONTRACT_LIMIT && mspeed > 0){
      is_rail_pos_limited = 1;
    }
    if(current_rail_pos > RAIL_EXPAND_LIMIT && mspeed < 0){
      is_rail_pos_limited = 1;
    }
  }


  if(is_rail_pos_limited == 0){
    Generalmotor(RAILCH, mspeed);
    output_rail_pos = mspeed;
  }else{
    Generalmotor(RAILCH, 0);
    output_rail_pos = 0;
  }
}

void StorePastRailPos(){
  pastpast_rail_pos = past_rail_pos;
  past_rail_pos= current_rail_pos;
}




float PID(float goal_val,float current_val, float past_val, float deltamsec, float Kp, float Ki, float Kd, float* p_sumoferr, float Ki_term_limit){
  float current_err = goal_val - current_val;
  float past_err = goal_val - past_val;
  float delta_err = (current_err - past_err) /deltamsec;
  float output;

  if(fabsf(Ki * (*p_sumoferr + (current_err + past_err)*deltamsec/2)) < Ki_term_limit)
    *p_sumoferr += (current_err + past_err)*deltamsec/2;

  output =  (Kp * current_err + Ki * (*p_sumoferr) + Kd * delta_err);
  return output;
}

void velPID(float goal_val,float current_val, float past_val, float pastpast_val, float deltamsec, float Kp, float Ki, float Kd, float* p_output){
  float current_err = goal_val - current_val;
  float past_err = goal_val - past_val;
  float pastpast_err = goal_val - pastpast_val;
  float delta_err = (current_err - past_err) /deltamsec;
  float past_delta_err = (past_err - pastpast_err) /deltamsec;
  float deltadelta_err = (delta_err - past_delta_err) / deltamsec;
  float delta_output;
  delta_output = Kp * delta_err + Ki * current_err * deltamsec + Kd * deltadelta_err / deltamsec;
  *p_output += delta_output;
}

void FireAngleMotor(float mspeed){
  if(mspeed > FIRE_ANGLE_LIMIT_DUTY){
    mspeed = FIRE_ANGLE_LIMIT_DUTY;
  }
  if(mspeed < -FIRE_ANGLE_LIMIT_DUTY){
    mspeed = -FIRE_ANGLE_LIMIT_DUTY;
  }

  if(HAL_GPIO_ReadPin(FIREANGLE_LIMSW_PORT, FIREANGLE_LIMSW_PIN) == GPIO_PIN_RESET){
    //Limit switch is active
    if(mspeed < 0){
      is_fire_angle_limited = 0;
    }else{
      is_fire_angle_limited = 1;
    }
  }else{
    //Limit switch is not active
    is_fire_angle_limited = 0;
  }

  if(is_fire_angle_set == 1){
    if(current_fire_angle > FIRE_ANGLE_UP_LIMIT && mspeed > 0){
      is_fire_angle_limited = 1;
    }
  }

  if(current_fire_angle < FIRE_ANGLE_DOWN_LIMIT && mspeed < 0){
    is_fire_angle_limited = 1;
  }


  if(is_fire_angle_limited == 0){
    output_fire_angle = mspeed;
    Generalmotor(FIREANGLECH, mspeed);
  }else{
    output_fire_angle = 0;
    Generalmotor(FIREANGLECH, 0);
  }
}

void UpdateShagaiFireAngle(float deg){
  static float past_time;
  static float sumoferr = 0;
  float output;
  float current_time = HAL_GetTick();
  float delta = (current_time - past_time);

  if(deg > 0){
    deg = 0;
  }
  if(deg < FIRE_ANGLE_DOWN){
    deg = FIRE_ANGLE_DOWN;
  }
  if(is_fire_angle_set != 1)
    return;
  output = PID(deg, current_fire_angle, past_fire_angle, delta, fire_angle_Kp, fire_angle_Ki, fire_angle_Kd, &sumoferr, fire_angle_Ki_term_limit);
  //velPID(deg, current_fire_angle, past_fire_angle, pastpast_fire_angle, delta, fire_angle_Kp, fire_angle_Ki, fire_angle_Kd, &output);

  FireAngleMotor(output);
}
void UpdateRail(float pos){
  static float past_time;
  static float sumoferr = 0;
  float output;
  float current_time = HAL_GetTick();
  float delta = (current_time - past_time);

  if(pos > RAIL_EXPAND_LIMIT){
    pos = RAIL_EXPAND_LIMIT;
  }
  if(pos < RAIL_CONTRACT_LIMIT){
    pos = RAIL_CONTRACT_LIMIT;
  }

  output = PID(pos, current_rail_pos, past_rail_pos, delta, rail_pos_Kp, rail_pos_Ki, rail_pos_Kd, &sumoferr, rail_Ki_term_limit);
  //velPID(pos, current_rail_pos, past_rail_pos, pastpast_rail_pos, delta, rail_pos_Kp, rail_pos_Ki, rail_pos_Kd, &output);

  output *= -1;

  RailMotor(output);
}
void UpdatePoker(float pos){
  static float past_time;
  static float sumoferr = 0;
  float output;
  float current_time = HAL_GetTick();
  float delta = (current_time - past_time);

  if(pos > POKER_EXPAND_LIMIT){
    pos = POKER_EXPAND_LIMIT;
  }
  if(pos < POKER_CONTRACT_LIMIT){
    pos = POKER_CONTRACT_LIMIT;
  }

  output = PID(pos, current_poker_pos, past_poker_pos, delta, poker_pos_Kp, poker_pos_Ki, poker_pos_Kd, &sumoferr, poker_Ki_term_limit);
  //velPID(pos, current_poker_pos, past_poker_pos, pastpast_poker_pos, delta, poker_pos_Kp, poker_pos_Ki, poker_pos_Kd, &output);

  if(fabsf(current_poker_pos - pos) < DEADZONE_OF_POKER){
    PokerMotor(0);
  }else{
   PokerMotor(output);
  }
}

void GetCurrentPokerPos(){
  static uint32_t ring_buff[POTENTIO_RINGBUFF_SIZE];
  static int8_t buff_index = 0;
  uint32_t move_ave = 0;

  ring_buff[buff_index] = adc2_val[2];

  for(int8_t i = 0; i < POTENTIO_RINGBUFF_SIZE; i++){
    move_ave += ring_buff[i];
  }
  move_ave /= POTENTIO_RINGBUFF_SIZE;

  buff_index = (buff_index + 1) % POTENTIO_RINGBUFF_SIZE;

  current_poker_pos = -1 * (((float)move_ave * POTENTIO_RANGE_DEG) / ADC_RESOLUTION) * POKER_POT_RADIUS * 2.0 * M_PI / 360.0;
}

void PokerMotor(float mspeed){
  is_poker_pos_limited = 0;

  if(mspeed > 0.5){
    mspeed = 0.5;
  }
  if(mspeed < -0.5){
    mspeed = -0.5;
  }

  if(current_poker_pos < POKER_CONTRACT_LIMIT && mspeed < 0){
    is_poker_pos_limited = 1;
  }
  if(current_poker_pos > POKER_EXPAND_LIMIT && mspeed > 0){
    is_poker_pos_limited = 1;
  }

  if(is_poker_pos_limited == 0){
    Generalmotor(POKERCH, mspeed);
    output_poker_pos = mspeed;
  }else{
    Generalmotor(POKERCH, 0);
    output_poker_pos = 0;
  }
}

void StorePastPokerPos(){
  pastpast_poker_pos = past_poker_pos;
  past_poker_pos = current_poker_pos;

}


void SetGergeAngle(float goaldeg){
  Servomotor(&SERVO_1, goaldeg);
  current_gerge_angle = goaldeg;
}

void ShagaiFire(int8_t isset){
 if(isset){
   Valve(FIRE_VALVE, GPIO_PIN_SET);
 }else{
   Valve(FIRE_VALVE, GPIO_PIN_RESET);
 }
}
void ShagaiHand(int8_t isheld){
  if(isheld){
    Valve(SHAGAI_HAND_VALVE, GPIO_PIN_SET);
  }else{
    Valve(SHAGAI_HAND_VALVE, GPIO_PIN_RESET);
  }

}

int8_t IsGergePushed(){
  if((HAL_GPIO_ReadPin(GERGEANGLE_LIMSW_PORT, GERGEANGLE_LIMSW_PIN) == GPIO_PIN_RESET)){
    return 1;
  }else{
    return 0;
  }
}

void ThrowShagai(){
  log_info("Fire");
  is_throwing = 1;
  ShagaiFire(1);
  HAL_Delay(FIRE_RELEASE_MSEC);
  log_info("Release");
  ShagaiHand(0);
  RailMotor(0);
//  is_rail_pos_set = 0;
  HAL_Delay(200);
  log_info("Fire Contract");
  ShagaiFire(0);
  is_throwing = 0;
}

void SetGoalPos(TrapezoidVariable* trapezoid ,float goal_pos){
  trapezoid->post_goalpos = goal_pos;

  float xd = trapezoid->post_goalpos - trapezoid->preexist_goalpos;
  // (v_max)^2 / a_max
  float vmax2_over_amax = trapezoid->max_velocity*trapezoid->max_velocity / trapezoid->max_acceleration;
  // v_max / a_max
  float vmax_over_amax = trapezoid->max_velocity / trapezoid->max_acceleration;
  //x_displacement / a_max
  float xd_over_amax = (trapezoid->post_goalpos - trapezoid->preexist_goalpos) / trapezoid->max_acceleration;
  //x_displacement / v_max
  float xd_over_vmax = (trapezoid->post_goalpos - trapezoid->preexist_goalpos) / trapezoid->max_velocity;


  trapezoid->msec_cnt = 0;
  if(fabs(trapezoid->post_goalpos - trapezoid->preexist_goalpos) > vmax2_over_amax){

    uint16_t msec = 0;
    for(uint16_t t = 0; t < vmax_over_amax * 1000; t++){
      float duration = (float)t / 1000;
      trapezoid->goal_val[t] = trapezoid->preexist_goalpos + SIGN(xd) * trapezoid->max_acceleration * duration * duration / 2;
    }

    msec = vmax_over_amax * 1000;

    for(uint16_t t = 0; t < (fabs(xd_over_vmax) - vmax_over_amax) * 1000; t++){
      float duration = (float)t / 1000;
      trapezoid->goal_val[msec + t] = trapezoid->preexist_goalpos + (SIGN(xd))*trapezoid->max_velocity * duration + (SIGN(xd))*vmax2_over_amax / 2;
    }

    msec = (vmax_over_amax+ fabs(xd_over_vmax) - vmax_over_amax) * 1000;

    for(uint16_t t = 0; t < vmax_over_amax * 1000; t++){
      float duration = (float)t / 1000;
      trapezoid->goal_val[msec + t] = trapezoid->preexist_goalpos + (SIGN(xd))*trapezoid->max_acceleration * duration*duration  / (-2) + (SIGN(xd))*trapezoid->max_velocity * duration + xd - (SIGN(xd))*(vmax2_over_amax /2);
    }

    msec = (vmax_over_amax+ fabs(xd_over_vmax) - vmax_over_amax + vmax_over_amax) * 1000;

    for(uint16_t t = msec; t < NUM_OF_TRAPEZOID_POINTS; t++){
      trapezoid->goal_val[t] = trapezoid->post_goalpos;
    }

  }else{
    uint16_t msec = 0;
    for(uint16_t t = 0; t < sqrt(fabs(xd_over_amax)) * 1000; t++){
      float duration = (float)t / 1000;
      trapezoid->goal_val[t] = trapezoid->preexist_goalpos + SIGN(xd) * trapezoid->max_acceleration * duration * duration / 2;
    }

    msec = sqrt(fabs(xd_over_amax)) * 1000;

    for(uint16_t t = 0; t < sqrt(fabs(xd_over_amax)) * 1000; t++){
      float duration = (float)t / 1000;
      trapezoid->goal_val[msec + t] = trapezoid->preexist_goalpos + (SIGN(xd))*trapezoid->max_acceleration * duration*duration / (-2) + (SIGN(xd))*sqrt(fabs(trapezoid->max_acceleration * xd)) * duration + xd/2;
    }

    msec = 2 * sqrt(fabs(xd_over_amax)) * 1000;

    for(uint16_t t = msec; t < NUM_OF_TRAPEZOID_POINTS; t++){
      trapezoid->goal_val[t] = trapezoid->post_goalpos;
    }
  }

}

void InitTrapezoid(TrapezoidVariable* trapezoid ,float current_pos, float max_acceleration, float max_velocity/*, volatile float* p_goal*/){
  trapezoid->state = REST;
  trapezoid->preexist_goalpos = current_pos;
  trapezoid->post_goalpos = current_pos;
  trapezoid->max_acceleration = max_acceleration;
  trapezoid->max_velocity = max_velocity;
  trapezoid->current_goal_val = current_pos;

  for(uint16_t i =0; i < NUM_OF_TRAPEZOID_POINTS; i++){
    trapezoid->goal_val[i] = current_pos;
  }

  trapezoid->buff.is_set = 0;
}

void UpdateGoalPos(TrapezoidVariable* trapezoid){
  trapezoid->current_goal_val = trapezoid->goal_val[trapezoid->msec_cnt];

  if(trapezoid->current_goal_val == trapezoid->post_goalpos){
    trapezoid->state = REST;
    trapezoid->preexist_goalpos = trapezoid->post_goalpos;
  }else{
    trapezoid->msec_cnt++;
    trapezoid->state = RUN;
  }
}

void SetGoalPosBuffer(TrapezoidVariable* trapezoid, float goal){
  trapezoid->buff.goal_pos_buff = goal;
  trapezoid->buff.is_set = 1;
}

void InitPokerTrapezoid(){
  InitTrapezoid(&poker_trapezoid, current_poker_pos, poker_max_acceleration,poker_max_velocity/*,&goal_poker_pos*/);
}

void UpdatePokerGoalPos(){
  UpdateGoalPos(&poker_trapezoid);
  UpdatePoker(poker_trapezoid.current_goal_val);
  goal_poker_pos = poker_trapezoid.current_goal_val;
}

void SetPokerGoalPos(float goal){
  if(poker_trapezoid.state == REST){
    SetGoalPos(&poker_trapezoid, goal);
  }else{
    SetGoalPosBuffer(&poker_trapezoid, goal);
  }
}

void InitRailTrapezoid(){
  InitTrapezoid(&rail_trapezoid, current_rail_pos, rail_max_acceleration, rail_max_velocity);
}

void UpdateRailGoalPos(){
  if(is_rail_pos_set != 1)
      return;
  UpdateGoalPos(&rail_trapezoid);
  UpdateRail(rail_trapezoid.current_goal_val);
  goal_rail_pos = rail_trapezoid.current_goal_val;
}

void SetRailGoalPos(float goal){
  if(rail_trapezoid.state == REST){
    SetGoalPos(&rail_trapezoid, goal);
  }else{
    SetGoalPosBuffer(&rail_trapezoid, goal);
  }
}

void InitFireAngleTrapezoid(){
  InitTrapezoid(&fire_angle_trapezoid, current_fire_angle, fire_angle_max_acceleration, fire_angle_max_velocity);
}

void UpdateFireAngleGoalPos(){
  if(is_fire_angle_set != 1)
    return;
  UpdateGoalPos(&fire_angle_trapezoid);
  UpdateShagaiFireAngle(fire_angle_trapezoid.current_goal_val);
  goal_fire_angle = fire_angle_trapezoid.current_goal_val;
}

void SetFireAngleGoalPos(float goal){
  if(fire_angle_trapezoid.state == REST){
    SetGoalPos(&fire_angle_trapezoid, goal);
  }else{
    SetGoalPosBuffer(&fire_angle_trapezoid, goal);
  }
}

void DisplayOnLED(uint8_t val){
  HAL_GPIO_WritePin(LED1_GPIO_Port,LED1_Pin,val&0b0001);
  HAL_GPIO_WritePin(LED2_GPIO_Port,LED2_Pin,val&0b0010);
  HAL_GPIO_WritePin(LED3_GPIO_Port,LED3_Pin,val&0b0100);
  HAL_GPIO_WritePin(LED4_GPIO_Port,LED4_Pin,val&0b1000);
}

void CheckFireTimingButton(){
  if(HAL_GPIO_ReadPin(BUTTON1_GPIO_Port, BUTTON1_Pin) == GPIO_PIN_SET){
    release_msec_offset += 1;
  }

  if(HAL_GPIO_ReadPin(BUTTON3_GPIO_Port, BUTTON3_Pin) == GPIO_PIN_SET){
    release_msec_offset -= 1;
  }

  release_msec_offset = fmax(fmin(release_msec_offset, 0b0111), -7);
}



