/*
 * user_interface.cpp
 *
 *  Created on: 2018/05/20
 *      Author: Ryohei
 */


#include "user_interface.h"

/*-----------------------------------------------
 * 汎用タイマ
 ----------------------------------------------*/
/*
 * マイクロ秒タイマ
 * @param	t : 待ち時間(us)
 * @return
 */
/*
void DelayMicroSec(uint32_t t) {
	HAL_TIM_Base_Start_IT(&MICROSEC_TIM_HANDLER);
	while (t--) {
		while (__HAL_TIM_GET_FLAG(&MICROSEC_TIM_HANDLER, TIM_FLAG_UPDATE) == 0) ;
		__HAL_TIM_CLEAR_FLAG(&MICROSEC_TIM_HANDLER, TIM_FLAG_UPDATE);
	}
	HAL_TIM_Base_Stop_IT(&MICROSEC_TIM_HANDLER);
}
*/

/*-----------------------------------------------
 * LED・ブザー表示パターン
 ----------------------------------------------*/
/*
 * 警告表示LED
 * @param
 * @return
 */
void UIWarning(void) {
	for (uint8_t i = 0; i < 3; i++) {
		UILEDSet(0b1111);
		HAL_Delay(200);
//		BuzzerBeep(BUZZER_A7, 200);
		UILEDSet(0);
		HAL_Delay(50);
	}
}

/*
 * モード決定
 * @param
 * @return
 */
void UIModeExec(void) {
//	BuzzerBeep(BUZZER_C7, 100);
	HAL_Delay(10);
//	BuzzerBeep(BUZZER_E7, 100);
	HAL_Delay(10);
//	BuzzerBeep(BUZZER_G7, 100);
}

/*
 * モード設定失敗時に戻る用
 * @param
 * @return
 */
void UIModeBack(void) {
//	BuzzerBeep(BUZZER_G7, 100);
	HAL_Delay(10);
//	BuzzerBeep(BUZZER_E7, 100);
	HAL_Delay(10);
//	BuzzerBeep(BUZZER_C7, 100);
}


/*-----------------------------------------------
 * UILED
 ----------------------------------------------*/
/*
 * 指定にしたがってUILEDを点灯
 * @param	led : LEDを2進数の各ビットに見立てた時の値(ex:UILEDSet(0b1010))
 * @return
 */
void UILEDSet(uint8_t led){
	HAL_GPIO_WritePin(UILED_BIT1_PORT, UILED_BIT1_PIN, ((led & 0b00000001) ? (GPIO_PIN_SET) : (GPIO_PIN_RESET)));
	HAL_GPIO_WritePin(UILED_BIT2_PORT, UILED_BIT2_PIN, ((led & 0b00000010) ? (GPIO_PIN_SET) : (GPIO_PIN_RESET)));
	HAL_GPIO_WritePin(UILED_BIT3_PORT, UILED_BIT3_PIN, ((led & 0b00000100) ? (GPIO_PIN_SET) : (GPIO_PIN_RESET)));
	HAL_GPIO_WritePin(UILED_BIT4_PORT, UILED_BIT4_PIN, ((led & 0b00001000) ? (GPIO_PIN_SET) : (GPIO_PIN_RESET)));
}


/*-----------------------------------------------
 * Buzzer
 ----------------------------------------------*/
/*
 * ブザーを指定された周波数で指定された時間鳴らす
 * @param	freq : 周波数(Hz)
 * 			time : 時間(ms)
 * 			(ex:BuzzerBeep(BUZZER_A7, 1000))
 * @return
 */
void BuzzerBeep(int16_t freq, int32_t time) {
	BuzzerOn(freq);
	HAL_Delay(time);
	BuzzerOff();
}

/*
 * ブザーを指定された周波数で鳴らす
 * @param
 * @return
 */
void BuzzerOn(int16_t freq) {
	BUZZER_TIM_HANDLER.Init.Period = (float)BUZZER_TIM_FREQ / (float)freq;
	HAL_TIM_Base_Init(&BUZZER_TIM_HANDLER);
	HAL_TIM_PWM_Init(&BUZZER_TIM_HANDLER);

	__HAL_TIM_SetCompare(&BUZZER_TIM_HANDLER, BUZZER_TIM_CHANNEL, (float)BUZZER_TIM_FREQ / ((float)freq * 2.0f));

	//PWMスタート
	HAL_TIM_PWM_Start(&BUZZER_TIM_HANDLER, BUZZER_TIM_CHANNEL);
}

/*
 * ブザー止める
 * @param
 * @return
 */
void BuzzerOff(void) {
	HAL_TIM_PWM_Stop(&BUZZER_TIM_HANDLER, BUZZER_TIM_CHANNEL);
}

void Melody(uint8_t value){
	switch(value){
		case 0:
			//風呂焚き
			BuzzerBeep(BUZZER_G6,ONE_8);
			BuzzerBeep(BUZZER_F6,ONE_8);
			BuzzerBeep(BUZZER_E6,ONE_4);
			BuzzerBeep(BUZZER_G6,ONE_8);
			BuzzerBeep(BUZZER_C7,ONE_8);
			BuzzerBeep(BUZZER_B6,ONE_4);
			BuzzerBeep(BUZZER_G6,ONE_8);
			BuzzerBeep(BUZZER_D7,ONE_8);
			BuzzerBeep(BUZZER_C7,ONE_4);
			BuzzerBeep(BUZZER_E7,ONE_4);
			HAL_Delay(ONE_4);
			BuzzerBeep(BUZZER_C7,ONE_8);
			BuzzerBeep(BUZZER_B6,ONE_8);
			BuzzerBeep(BUZZER_A6,ONE_4);
			BuzzerBeep(BUZZER_F7,ONE_8);
			BuzzerBeep(BUZZER_D7,ONE_8);
			BuzzerBeep(BUZZER_C7,ONE_4);
			BuzzerBeep(BUZZER_B6,ONE_4);
			BuzzerBeep(BUZZER_C7,ONE_2);
			break;
		case 1:
			//ハンガリー舞曲
			BuzzerBeep(BUZZER_D6,ONE_4+ONE_8);
			BuzzerBeep(BUZZER_G6,ONE_8);
			BuzzerBeep(BUZZER_Bm6,ONE_4+ONE_8);
			BuzzerBeep(BUZZER_G6,ONE_8);
			BuzzerBeep(BUZZER_FM6,ONE_4+ONE_8);
			BuzzerBeep(BUZZER_G6,ONE_16);
			BuzzerBeep(BUZZER_A6,ONE_16);
			BuzzerBeep(BUZZER_G6,ONE_2);
			break;
		case 10:
			//四季　春
			BuzzerBeep(BUZZER_E6,ONE_8);
			BuzzerBeep(BUZZER_GM6,ONE_8);
			BuzzerBeep(BUZZER_GM6,ONE_8);
			BuzzerBeep(BUZZER_GM6,ONE_8);
			BuzzerBeep(BUZZER_FM6,ONE_16);
			BuzzerBeep(BUZZER_E6,ONE_16);
			BuzzerBeep(BUZZER_B6,ONE_4+ONE_8);

			BuzzerBeep(BUZZER_B6,ONE_16);
			BuzzerBeep(BUZZER_A6,ONE_16);
			BuzzerBeep(BUZZER_GM6,ONE_8);
			BuzzerBeep(BUZZER_GM6,ONE_8);
			BuzzerBeep(BUZZER_GM6,ONE_8);
			BuzzerBeep(BUZZER_FM6,ONE_16);
			BuzzerBeep(BUZZER_E6,ONE_16);
			BuzzerBeep(BUZZER_B6,ONE_4+ONE_8);

			BuzzerBeep(BUZZER_B6,ONE_16);
			BuzzerBeep(BUZZER_A6,ONE_16);
			BuzzerBeep(BUZZER_GM6,ONE_8);
			BuzzerBeep(BUZZER_A6,ONE_16);
			BuzzerBeep(BUZZER_B6,ONE_16);
			BuzzerBeep(BUZZER_A6,ONE_8);
			BuzzerBeep(BUZZER_GM6,ONE_8);
			BuzzerBeep(BUZZER_FM6,ONE_2);
			break;
		case 3:
			//運命
			BuzzerBeep(BUZZER_G6,ONE_8);
			BuzzerBeep(BUZZER_G6,ONE_8);
			BuzzerBeep(BUZZER_G6,ONE_8);
			BuzzerBeep(BUZZER_E6,ONE_2+ONE_4);
			BuzzerBeep(BUZZER_F6,ONE_8);
			BuzzerBeep(BUZZER_F6,ONE_8);
			BuzzerBeep(BUZZER_F6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_2+ONE_4);
			break;
		case 4:
			  //愛の挨拶
			BuzzerBeep(BUZZER_FM6,ONE_4);
			BuzzerBeep(BUZZER_A5,ONE_8);
			BuzzerBeep(BUZZER_FM6,ONE_8);
			BuzzerBeep(BUZZER_E6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_CM6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_G6,ONE_4);
			BuzzerBeep(BUZZER_G6,ONE_4);
			BuzzerBeep(BUZZER_G6,ONE_4);
			BuzzerBeep(BUZZER_A5,ONE_4);

			BuzzerBeep(BUZZER_FM6,ONE_4);
			BuzzerBeep(BUZZER_AM5,ONE_8);
			BuzzerBeep(BUZZER_FM6,ONE_8);
			BuzzerBeep(BUZZER_E6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_CM6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_E6,ONE_4);
			BuzzerBeep(BUZZER_E6,ONE_4);
			BuzzerBeep(BUZZER_E6,ONE_4);
			BuzzerBeep(BUZZER_F6,ONE_4);

			BuzzerBeep(BUZZER_FM6,ONE_4);
			BuzzerBeep(BUZZER_A5,ONE_8);
			BuzzerBeep(BUZZER_FM6,ONE_8);
			BuzzerBeep(BUZZER_E6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_CM6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_B6,ONE_4);
			BuzzerBeep(BUZZER_B6,ONE_4);
			BuzzerBeep(BUZZER_B6,ONE_4);

			BuzzerBeep(BUZZER_A6,ONE_8);
			BuzzerBeep(BUZZER_G6,ONE_8);
			BuzzerBeep(BUZZER_FM6,ONE_4);
			BuzzerBeep(BUZZER_E6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_B5,ONE_4);
			BuzzerBeep(BUZZER_CM6,ONE_4);
			BuzzerBeep(BUZZER_D6,ONE_2);
			break;
		case 5:
			//マクド
			for(int i=0;i<6;i++){
				BuzzerBeep(BUZZER_G5,ONE_16);
				HAL_Delay(ONE_16+ONE_8);
				BuzzerBeep(BUZZER_G5,ONE_8);
				BuzzerBeep(BUZZER_F5,ONE_8);
			}
			break;
		case 6:
			//フーガ
			BuzzerBeep(BUZZER_A6,ONE_8);
			BuzzerBeep(BUZZER_G6,ONE_8);
			BuzzerBeep(BUZZER_A6,ONE_2+ONE_4);
			BuzzerBeep(BUZZER_G6,ONE_8);
			BuzzerBeep(BUZZER_F6,ONE_8);
			BuzzerBeep(BUZZER_E6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_CM6,ONE_4);
			BuzzerBeep(BUZZER_D6,ONE_2);
			break;
		case 7:
			//チャルメラ
			BuzzerBeep(BUZZER_C6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_E6,ONE_4+ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_C6,ONE_8);
			HAL_Delay(ONE_8);
			BuzzerBeep(BUZZER_C6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_E6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_C6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8+ONE_2);
			break;
		case 8:
			//横断歩道？
			for(int i=0;i<4;i++){
				BuzzerBeep(BUZZER_G6,ONE_16);
				HAL_Delay(ONE_16);
				BuzzerBeep(BUZZER_E6,ONE_8);
				HAL_Delay(ONE_8);
				BuzzerBeep(BUZZER_G6,ONE_32);
				HAL_Delay(ONE_32);
				BuzzerBeep(BUZZER_G6,ONE_32);
				HAL_Delay(ONE_32);
				BuzzerBeep(BUZZER_E6,ONE_8);
				HAL_Delay(ONE_8);
			}
			break;
		case 9:
			//ファミマ
			BuzzerBeep(BUZZER_FM6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_A5,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_E6,ONE_8);
			BuzzerBeep(BUZZER_A5,ONE_8);
			HAL_Delay(ONE_8);
			BuzzerBeep(BUZZER_F6,ONE_8);
			BuzzerBeep(BUZZER_E6,ONE_8);
			BuzzerBeep(BUZZER_FM6,ONE_8);
			BuzzerBeep(BUZZER_E6,ONE_8);
			BuzzerBeep(BUZZER_A5,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			break;
		case 2:
			//「不協和音」
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_E6,ONE_8);
			BuzzerBeep(BUZZER_F6,ONE_8);
			BuzzerBeep(BUZZER_E6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_E6,ONE_16);
			BuzzerBeep(BUZZER_F6,ONE_16);
			HAL_Delay(ONE_16);
			BuzzerBeep(BUZZER_F6,ONE_16);
			BuzzerBeep(BUZZER_E6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_E6,ONE_8);
			BuzzerBeep(BUZZER_F6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_G6,ONE_8);
			BuzzerBeep(BUZZER_G6,ONE_16);
			BuzzerBeep(BUZZER_G6,ONE_8);
			BuzzerBeep(BUZZER_F6,ONE_16);
			BuzzerBeep(BUZZER_E6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_2);
			break;
		default:
			break;
	}
}

/*-----------------------------------------------
 * Button
 ----------------------------------------------*/
/*
 * ボタンのON/OFFを返す
 * @param	button_num : ボタンの番号1~3
 * @return	PUSHSW_ON/PUSHSW_OFF
 */
int8_t ButtonIsOn(uint8_t button_num) {
	//存在しないボタンを指定している場合
	if (button_num > NUM_OF_BUTTON) {
		return BUTTON_NUM_ERROR;
	}

	if (button_num == 1) {
#ifdef BUTTON_1_PULL_UP
		if (HAL_GPIO_ReadPin(BUTTON_1_PORT, BUTTON_1_PIN) == GPIO_PIN_SET) {return BUTTON_OFF;}
		else 															   {return BUTTON_ON;}
#endif
#ifdef BUTTON_1_PULL_DOWN
		if (HAL_GPIO_ReadPin(BUTTON_1_PORT, BUTTON_1_PIN) == GPIO_PIN_SET) {return BUTTON_ON;}
		else 															   {return BUTTON_OFF;}
#endif
	} else if (button_num == 2) {
#ifdef BUTTON_2_PULL_UP
		if (HAL_GPIO_ReadPin(BUTTON_2_PORT, BUTTON_2_PIN) == GPIO_PIN_SET) {return BUTTON_OFF;}
		else 															   {return BUTTON_ON;}
#endif
#ifdef BUTTON_2_PULL_DOWN
		if (HAL_GPIO_ReadPin(BUTTON_2_PORT, BUTTON_2_PIN) == GPIO_PIN_SET) {return BUTTON_ON;}
		else 															   {return BUTTON_OFF;}
#endif
	} else if (button_num == 3) {
#ifdef BUTTON_3_PULL_UP
		if (HAL_GPIO_ReadPin(BUTTON_3_PORT, BUTTON_3_PIN) == GPIO_PIN_SET) {return BUTTON_OFF;}
		else 															   {return BUTTON_ON;}
#endif
#ifdef BUTTON_3_PULL_DOWN
		if (HAL_GPIO_ReadPin(BUTTON_3_PORT, BUTTON_3_PIN) == GPIO_PIN_SET) {return BUTTON_ON;}
		else 															   {return BUTTON_OFF;}
#endif
	}

	//ここには来ないハズ
	else {
		return BUTTON_NUM_ERROR;
	}
}

/*
 * ボタンがOFFになるまで待って、長押し/短押しを返す
 * @param	button_num : ボタンの番号0~2
 * 			long_short_thres : 長押し/短押しの閾値(ms)
 * @return	BUTTON_LONG/BUTTON_SHORT
 */
int8_t ButtonJudgePushTime(uint8_t button_num, uint32_t long_short_thres) {
	//存在しないボタンを指定している場合
	if (button_num > NUM_OF_BUTTON - 1) {
		return BUTTON_NUM_ERROR;
	}

	volatile uint32_t push_time = 50;				//押し時間タイマ
	HAL_Delay(push_time);							//チャタリング防止
	while (ButtonIsOn(button_num) == BUTTON_ON) {	//ボタンが押されている間カウント
		push_time++;
		HAL_Delay(1);
	}
	if (push_time > long_short_thres) return BUTTON_LONG;
	else							  return BUTTON_SHORT;
}
